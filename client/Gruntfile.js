module.exports = function (grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        //Create jsResources and cssResources configuration properties
        jsResources: [],
        cssResources: [],

        clean: {
            build: {
                src: ['./release/**/*']
            }

        },

        copy: {
            build: {
                files: [{
                    cwd: './src/',
                    dest: './release/',
                    expand: true,
                    //Copy over the index.html file and all the files in the "views" directory
                    src: ['index.html',
                        'image/**',
                        'node_modules/**',
                        'partials/**',
                        'vendor/**',
                        '50x.html',
                        'favicon.ico',
                    ]
                }]
            }
        },

        replace: {
            gather: {
                files: [{
                    cwd: './src/',
                    dest: './release/',
                    expand: true,
                    src: ['index.html']
                }],
                options: {
                    patterns: [{
                        //Grab the <!--build-js-start--> and <!--build-js-end--> comments and everything in-between
                        match: /\<\!\-\-build\-js\-start[\s\S]*build\-js\-end\-\-\>/,
                        replacement: function (matchedString) {
                            //Grab all of the src attributes from the <script> tags
                            var jsArray = matchedString.match(/(src\s?\=\s?[\'\"])([^\'\"]*)([\'\"])/g);
                            jsArray.forEach(function (element) {
                                //Get just the value of the src attribute (the file path to the JS file)
                                var resourceTarget = element.match(/(src\s?\=\s?[\'\"])([^\'\"]*)([\'\"])/)[2];
                                var targetConfig = grunt.config('jsResources');
                                //Alter the path for use with the concat task
                                targetConfig.push('./src/' + resourceTarget);
                                //Add the path to the JS file to the jsResources configuration property
                                grunt.config('jsResources', targetConfig);
                            });

                            //Replace the entire build-js-start to build-js-end block with this <script> tag
                            var version = grunt.config.get('pkg.version');
                            return '<script type="text/javascript" src="app.min.js?v=' + version + '"></script>';
                        }
                    }, {
                            //Grab the <!--build-js-start--> and <!--build-js-end--> comments and everything in-between
                            match: /\<\!\-\-build\-css\-start[\s\S]*build\-css\-end\-\-\>/,
                            replacement: function (matchedString) {
                                //Grab all of the href attributes from the <href> tags
                                var cssArray = matchedString.match(/(href\s?\=\s?[\'\"])([^\'\"]*)([\'\"])/g);
                                cssArray.forEach(function (element) {
                                    var resourceTarget = element.match(/(href\s?\=\s?[\'\"])([^\'\"]*)([\'\"])/)[2];
                                    var targetConfig = grunt.config('cssResources');
                                    //Alter the path for use with the concat task
                                    targetConfig.push('./src/' + resourceTarget);
                                    //Add the path to the CSS file to the cssResources configuration property
                                    grunt.config('cssResources', targetConfig);
                                });

                                //Replace the entire build-css-start to build-css-end block with this <link> tag
                                var version = grunt.config.get('pkg.version');
                                return '<link rel="stylesheet" href="main.min.css?v=' + version + '"/>';
                            }
                        }]
                }
            }
        },

        concat: {
            js: {
                //Concatenate all of the files in the jsResources configuration property
                src: ['<%= jsResources %>'],
                dest: './release/app.min.js',
                options: {
                    separator: ';'
                }
            },

            css: {
                //Concatenate all of the files in the cssResources configuration property
                src: ['<%= cssResources %>'],
                dest: './release/main.min.css'
            }

        },
        uglify: {
            options: {
                mangle: false
            },
            dist: {
                src: './release/app.min.js',
                dest: './release/app.min.js'
            }
        },
        cssmin: {
            target: {
                files: [{
                    expand: true,
                    src: ['./release/main.min.css'],
                    dest: './',
                }]
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-replace');
    grunt.loadNpmTasks('grunt-contrib-cssmin');

    grunt.registerTask('build', 'Creates a build from the files in the app directory', function () {
        //So the user doesn't have to add '--force' to the command to clean the build directory
        grunt.option('force', true);

        grunt.task.run([
            'clean:build',
            'copy:build',
            'replace:gather',
            'concat:css',
            'concat:js',
            'uglify',
            'cssmin'
        ]);

    });
};
