angular.module('photoFeedApp').controller('mainCtrl', ['$rootScope', '$scope', 'requestService', '$window',
    function($rootScope, $scope, requestService, $window) {
    	$scope.isAuthenticated = function() {
            return requestService.requestAuthStat();
        };

        $scope.requestUserData = function() {
            requestService.requestGet("/api/users/self").then(function(res){
                $scope.userName = res.data[0].username;
                $rootScope.userId = res.data[0].id;
            });
            var logo = angular.element(document.querySelector('#logo'))[0];
            logo.style.display = "none";
        };

        $scope.instagramLogin = function() {
            requestService.requestPost("/api/auth/instagram").then(function(res){
                $window.location.href = res.data.url;
            });
        };
    }
]);