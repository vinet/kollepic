/* All right reserved by PhotoFeed */

package com.photofeed.domain;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class LocationSearchResult implements IResult{
    private int count;
    private List<Location> locations= new ArrayList<>();

    @Override
    public void convertJsonToObject(String json) {
        JSONObject obj = new JSONObject(json);
        JSONArray array = obj.getJSONArray("data");
        if (array == null || array.length() == 0) {
            return;
        }

        count = array.length();
        for (int i = 0; i < count; i++) {
            JSONObject idxObject = array.getJSONObject(i);
            String id = idxObject.getString("id");
            Double latitude = idxObject.getDouble("latitude");
            Double longitude = idxObject.getDouble("longitude");
            String name = idxObject.getString("name");
            locations.add(new Location(id, latitude, longitude, name));
        }
    }

    public int getCount() {
        return count;
    }

    public List<String> getTopKCount(int k) {
        List<Location> closestKLocations = locations.subList(0, k + 1);
        if (closestKLocations == null || closestKLocations.size() == 0) {
            return new ArrayList<>();
        } else {
            return closestKLocations.stream().map(location -> "#" + location.name +
                                                              "#" + location.id +
                                                              "#" + location.latitude +
                                                                "#" + location.longitude
            ).collect(Collectors.toList());
        }
    }

    @Override
    public String toString() {
        int i = 0;
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        for (Location location: locations) {
            if (i > 0) {
                sb.append(",");
            }
            sb.append(String.format("\n\t%s\t%f\t%f\t%s", location.id, location.latitude,
                                                    location.longitude, location.name));
            i++;
        }
        sb.append("\n}");
        return sb.toString();
    }

    class Location implements Comparable<Location> {
        String id;
        double longitude;
        double latitude;
        String name;

        public Location(String id, double longitude, double latitude, String name) {
            this.id = id;
            this.longitude = longitude;
            this.latitude = latitude;
            this.name = name;
        }

        @Override
        public int compareTo(Location t) {
            return this.id.compareTo(t.id);
        }
    }
}
