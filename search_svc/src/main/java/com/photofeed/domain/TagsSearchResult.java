/* All right reserved by PhotoFeed */
package com.photofeed.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.json.JSONArray;
import org.json.JSONObject;

public class TagsSearchResult implements IResult {
	private int count;
	private List<Tag> tags = new ArrayList<>();

	@Override
	public void convertJsonToObject(String json) {
		JSONObject obj = new JSONObject(json);
		JSONArray array = obj.getJSONArray("data");
		if (array == null || array.length() == 0) {
			return;
		}

		count = array.length();
		for (int i = 0; i < count; i++) {
			JSONObject idxObject = array.getJSONObject(i);
			String name = idxObject.getString("name");
			int media_count = idxObject.getInt("media_count");
			tags.add(new Tag(name, media_count));
		}
	}

	public int getCount() {
		return count;
	}

	public List<String> getTopKCount(int k) {
		List<Tag> rankedTags = tags.subList(0, k + 1);
		if (rankedTags == null || rankedTags.size() == 0) {
			return new ArrayList<>();
		} else {
			return rankedTags.stream().map(tag -> "#" + tag.name).collect(Collectors.toList());
		}
	}

	@Override
	public String toString() {
		int i = 0;
		StringBuilder sb = new StringBuilder();
		sb.append("{");
		for (Tag tag: tags) {
			if (i > 0) {
				sb.append(",");
			}
			sb.append(String.format("\n\t%s: %s", tag.name, tag.media_count));
			i++;
		}
		sb.append("\n}");
		return sb.toString();
	}

	class Tag implements Comparable<Tag> {
		String name;
		int media_count;

		public Tag(String name, int media_count) {
			this.name = name;
			this.media_count = media_count;
		}

		@Override
		public int compareTo(Tag t) {
			return this.media_count - t.media_count;
		}
	}
}
