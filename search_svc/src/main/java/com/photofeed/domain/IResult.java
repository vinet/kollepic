/* All right reserved by PhotoFeed */
package com.photofeed.domain;

public interface IResult {
	void convertJsonToObject(String json);
}
