/* All right reserved by PhotoFeed */
package com.photofeed.search;


import com.photofeed.search.impl.SimpleServiceFactory;
import com.photofeed.SearchSvc.SearchService;
import org.apache.thrift.server.TServer;
import org.apache.thrift.server.TThreadPoolServer;
import org.apache.thrift.transport.TServerSocket;
import org.apache.thrift.transport.TServerTransport;
import com.photofeed.util.SearchServiceProperties;

public class SearchServer {
    public static SimpleServiceFactory handler;
    public static SearchService.Processor processor;
    private static SearchServiceProperties searchServiceProperties = SearchServiceProperties.getInstance();

    public static void main(String[] args) {
        handler = new SimpleServiceFactory();
        processor = new SearchService.Processor(handler);

        Runnable simple = new Runnable() {
            public void run() {
                simple(processor);
            }
        };
        new Thread(simple).start();
    }

    public static void simple(SearchService.Processor processor) {
        try {
            TServerTransport serverTransport = new TServerSocket(searchServiceProperties.getServerPort());
            TServer server = new TThreadPoolServer(new TThreadPoolServer.Args(serverTransport).processor(processor));
            server.serve();
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }

}

