/* All right reserved by PhotoFeed */
package com.photofeed.search.impl;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.photofeed.domain.LocationSearchResult;
import com.photofeed.restclient.RestClientFactory;
import com.photofeed.SearchSvc.SearchService;
import org.apache.http.client.utils.URIBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class LocationSearchImpl implements SearchService.Iface {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public List<String> search(String access_token, Map<String,String> query) {
        RestClientFactory restClient = new RestClientFactory();
        try {
            URI uri = new URIBuilder("https://api.instagram.com/v1/locations/search").build();
            // should analyze query at this line

            Map<String, String> params = new HashMap<String, String>();
            if(query.containsKey("lat"))
                params.put("lat", query.get("lat"));
            if(query.containsKey("lng"))
                params.put("lng", query.get("lng"));
            params.put("access_token", access_token);

            LocationSearchResult locationsSearchResult = restClient.doGet(uri, params,
                    LocationSearchResult.class);

            if (locationsSearchResult == null) {
                logger.info("Tags search result is empty");
                return new ArrayList<>();
            }

            logger.debug("Tags search result: " + locationsSearchResult);
            return locationsSearchResult.getTopKCount(10);
        } catch (URISyntaxException e) {
            logger.error("Exception: ", e);
            return new ArrayList<>();
        }
    }

}
