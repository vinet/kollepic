package com.photofeed.search.impl;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.photofeed.domain.TagsSearchResult;
import com.photofeed.restclient.RestClientFactory;
import com.photofeed.SearchSvc.MissingParameterException;
import com.photofeed.SearchSvc.SearchService;
import org.apache.http.client.utils.URIBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TagsSearchImpl implements SearchService.Iface{
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	public List<String> search(String access_token, Map<String, String> query) throws MissingParameterException {
		RestClientFactory restClient = new RestClientFactory();
		try {
			URI uri = new URIBuilder("https://api.instagram.com/v1/tags/search").build();
			if (query == null) {
				throw new MissingParameterException("query.containsKey('tag') == false ", "invalid parameter");
			}
			// should analyze query at this line

			Map<String, String> params = new HashMap<String, String>();
			// check invalidation of key
			if(query.containsKey("tag"))
			params.put("q", query.get("tag"));
			params.put("access_token", access_token);

			TagsSearchResult tagsSearchResult = restClient.doGet(uri, params,
					TagsSearchResult.class);

			if (tagsSearchResult == null) {
				logger.info("Tags search result is empty");
				return new ArrayList<>();
			}

			logger.debug("Tags search result: " + tagsSearchResult);
			return tagsSearchResult.getTopKCount(10);
		} catch (URISyntaxException e) {
			logger.error("Exception: ", e);
			return new ArrayList<>();
		}
	}
}
