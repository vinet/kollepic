/* All right reserved by PhotoFeed */
package com.photofeed.search.impl;

import com.photofeed.SearchSvc.InvalidSearchOpException;
import com.photofeed.SearchSvc.SearchService;
import org.apache.thrift.TException;

import java.util.List;
import java.util.Map;

public class SimpleServiceFactory implements SearchService.Iface{

    @Override
    public List<String> search(String access_token, Map<String, String> query) throws TException {


        if(query.containsKey("op") == false) {
            throw new InvalidSearchOpException("null", "empty option");
        }

        String op = query.remove("op");
        SearchService.Iface service = SimpleServiceFactory.getService(op);
        if(service == null) {
           throw new InvalidSearchOpException("op", "option is not valid");
        }

        return service.search(access_token, query);
    }

    // build a map<tag, SearchService.class>
    // return map.get(op).newinstance();
    public static SearchService.Iface getService(String op) {
        op = op.toLowerCase();
        if(op.equals("tag")) {
            return new TagsSearchImpl();
        }
        else if(op.equals("location")) {
            return new LocationSearchImpl();
        }
        else {
            return null;
        }
    }
}

