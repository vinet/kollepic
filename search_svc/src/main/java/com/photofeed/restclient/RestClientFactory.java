package com.photofeed.restclient;

import java.io.IOException;
import java.net.URI;
import java.util.Map;

import com.photofeed.domain.IResult;
import com.photofeed.util.SearchServiceProperties;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Rest client factory to make synchronous REST API call
 */
public class RestClientFactory {
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	private CloseableHttpClient httpclient;
	private ThreadPoolConnectionManager tm = ThreadPoolConnectionManager.getInstance();
	private SearchServiceProperties searchServiceProperties = SearchServiceProperties.getInstance();

	public RestClientFactory() {
		RequestConfig requestConfig = RequestConfig.custom()
			  .setConnectTimeout(searchServiceProperties.getConnectTimeout())
			  .setSocketTimeout(searchServiceProperties.getSocketTimeout())
			  .setConnectionRequestTimeout(searchServiceProperties.getConnectionRequestTimeout())
			  .build();

		httpclient = HttpClients.custom()
			  .setConnectionManager(tm.getPoolingHttpClientConnectionManage())
			  .setDefaultRequestConfig(requestConfig)
			  .setConnectionManagerShared(true)
			  .build();
	}

	/**
	 * Make GET REST API call
	 * @param uri request endpoint
	 * @param queryParameters get request parameters
	 * @param clazz return type class
	 * @return response object
	 */
	public <T extends IResult> T doGet(URI uri, Map<String, String> queryParameters, Class<T> clazz) {
		URIBuilder uriBuilder = new URIBuilder()
			  .setScheme(uri.getScheme())
			  .setHost(uri.getHost())
			  .setPath(uri.getPath());
		if (uri.getPort() != -1) {
			uriBuilder.setPort(uri.getPort());
		}

		for (Map.Entry<String, String> entry: queryParameters.entrySet()) {
			uriBuilder.setParameter(entry.getKey(), entry.getValue());
		}

		try {
			URI requestURI = uriBuilder.build();
			HttpGet httpGet = new HttpGet(requestURI);
			HttpResponse httpResponse = httpclient.execute(httpGet);
			int statusCode = httpResponse.getStatusLine().getStatusCode();

			if (statusCode >= 200 && statusCode < 300) {
				return parse(httpResponse.getEntity(), clazz);
			} else {
				logger.error("Http Get {}, return statusCode {}", requestURI, statusCode);
				return null;
			}
		} catch (Exception e) {
			logger.error("Http Get IOException: ", e);
			return null;
		} finally {
			try {
				httpclient.close();
			} catch (IOException e) {
				logger.error("Close httpClient IOException: ", e);
			}
		}
	}

	private <T extends IResult> T parse(HttpEntity httpEntity, Class<T> clazz)
		  throws IOException, InstantiationException, IllegalAccessException {
		String res = EntityUtils.toString(httpEntity);
		if (res.length() == 0) {
			logger.debug("Response body is empty");
			return null;
		}

		String mime_type = ContentType.get(httpEntity).getMimeType();
		if (mime_type.equals(ContentType.APPLICATION_JSON.getMimeType())) {
			return convertJsonToObject(res, clazz);
		} else {
			// TODO: implement more content type if necessary
			return null;
		}
	}

	/**
	 * Convert json response to the given class object
	 * @param json response json
	 * @param clazz
	 */
	protected <T extends IResult> T convertJsonToObject(String json, Class<T> clazz)
		  throws IllegalAccessException, InstantiationException {
		T t = clazz.newInstance();
		t.convertJsonToObject(json);
		return t;
	}
}
