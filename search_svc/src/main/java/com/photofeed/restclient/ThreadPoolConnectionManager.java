package com.photofeed.restclient;

import com.photofeed.util.SearchServiceProperties;
import org.apache.http.HttpHost;
import org.apache.http.conn.routing.HttpRoute;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;

/**
 * Create {@link ThreadPoolConnectionManager} to manage both sync and async httpclient
 */
public class ThreadPoolConnectionManager {
	private static ThreadPoolConnectionManager tm = new ThreadPoolConnectionManager();
	private SearchServiceProperties searchServiceProperties = SearchServiceProperties.getInstance();

	private PoolingHttpClientConnectionManager cm;

	/**
	 * Get singleton ThreadPoolConnectionManager
	 * @return pool connection manager
	 */
	public static ThreadPoolConnectionManager getInstance() {
		return tm;
	}

	private ThreadPoolConnectionManager() {
		cm = new PoolingHttpClientConnectionManager();
		cm.setMaxTotal(searchServiceProperties.getMaxTotalConnectionCount());
		cm.setDefaultMaxPerRoute(searchServiceProperties.getDefaultMaxPerRouteCount());
		cm.setMaxPerRoute(new HttpRoute(new HttpHost(SearchServiceProperties.INSTAGRAM_API_URL)),
			  searchServiceProperties.getMaxPerRouteToInstagram());
	}

	public PoolingHttpClientConnectionManager getPoolingHttpClientConnectionManage() {
		return cm;
	}
}
