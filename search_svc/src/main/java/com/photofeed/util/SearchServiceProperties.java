/* All right reserved by PhotoFeed */
package com.photofeed.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SearchServiceProperties {
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	private static SearchServiceProperties searchServiceProperties = new SearchServiceProperties();
	private Properties props;

	public static String INSTAGRAM_API_URL = "api.instagram.com";

	public static SearchServiceProperties getInstance() {
		return searchServiceProperties;
	}

	private SearchServiceProperties() {
		props = new Properties();
		try (InputStream resourceStream = this.getClass().getResourceAsStream("/search_svc.properties")){
			props.load(resourceStream);
		} catch (IOException e) {
			logger.error("load properties file error: {}", e);
			props = null;
		}
	}

	public int getMaxTotalConnectionCount() {
		return getIntProperty("total.connection.count", 100);
	}

	public int getDefaultMaxPerRouteCount() {
		return getIntProperty("total.maxPerRount.count", 20);
	}

	public int getMaxPerRouteToInstagram() {
		return getIntProperty("total.maxPerRount.instagram.count", 80);
	}

	public int getConnectTimeout() {
		return getIntProperty("client.connect.timeout", 60 * 1000);
	}

	public int getSocketTimeout() {
		return getIntProperty("client.socket.timeout", 3 * 60 * 1000);
	}

	public int getConnectionRequestTimeout() {
		return getIntProperty("client.connection.request.timeout", 60 * 1000);
	}

	public int getServerPort() {
		return getIntProperty("server.port", 9000);
	}

	private int getIntProperty(String key, int defaultValue) {
		String value = props.getProperty(key);
		if (value == null) {
			return defaultValue;
		}
		try {
			return Integer.parseInt(value);
		} catch (NumberFormatException e) {
			return defaultValue;
		}
	}
}
