#!/bin/sh

echo_time() {
     date +"%Y-%m-%d %H:%M:%S - $(printf "%s " "$@" | sed 's/%/%%/g')"
}

stop_node() {
    node_inspector_pid=$(ps ax | grep "[n]ode-inspector" | awk '{print $1}')
    if [ -n "$node_inspector_pid" ]; then
        kill -9 $node_inspector_pid
        echo_time "kill node-inspector process "$node_inspector_pid
    fi
    node_pid=$(ps ax | grep "[n]ode" | awk '{print $1}')
    if [ -n "$node_pid" ]; then
        kill -9 $node_pid
        echo_time "kill node process "$node_pid
    fi
}

cd /home/ubuntu/photofeed

newcommit=$(git ls-remote origin master | awk '{print $1}')
commit=$(git log -1 --format="%H")

if [ -z "$newcommit" ] || [ -z "$commit" ]; then
    echo_time "Error: commit number cannot be empty!"
    exit 1
fi

echo_time "Start cron job to sync to latest code if necessary"

if [ $newcommit = $commit ]; then
    echo_time "No new commit"
else
    echo_time "New commit is pushed"

    stop_node

    /usr/bin/git pull --rebase origin master
    cd client/
    /usr/bin/npm install
    /usr/local/bin/grunt clean build
    cd ../web_server/
    /usr/bin/npm install
    /usr/local/bin/node-inspector &
    export NODE_TYPE='production'
    /usr/local/bin/node --debug server.js &
    echo_time "start web server"
fi
