
namespace java com.photofeed.SearchSvc

exception MissingParameterException {
	1:string param,
	2:string msg 
}

exception InvalidSearchOpException {
	1:string op,
	2:string msg
}

service SearchService {
   list<string> search(1: string access_token, 2:map<string, string> query) throws (1:MissingParameterException err, 2:InvalidSearchOpException op_err)

}

