var express = require('express');
var util = require('../util/util');
var webConfig = require('../conf/web');
var request = require('request');
var url = require('url');
var logger = require('../conf/log');
var userDao = require('../dao/userDao');
var favDao = require('../dao/favoriteDao');
var authDao = require('../dao/authDao');
var Timer = require('../util/timer');
var thrift = require('thrift');
var search_svc = require('../node-thrift/SearchService');
var ttype = require('../node-thrift/search_svc_types');
var router = express.Router();

var getDeployMode = function(conf, req) {
    return req.app.get('node_type') === 'development' ? conf.development : conf.production;
};

var querySetFavPhotosIfNeeded = function(userId, medias, res) {
    if (medias.length === 0) {
        res.status(200).end();
        return;
    }

    var mediaIds = [];
    medias.forEach(function(media) {
        mediaIds.push(media.id);
    });

    favDao.queryByUserIdMultiMediaIds(userId, mediaIds).then(
        function(favMediaIds) {
            medias.forEach(function(media) {
                if (favMediaIds.indexOf(media.id) === -1) {
                    media.user_favorite = false;
                } else {
                    media.user_favorite = true;
                }
            });
            res.json(medias);
        },
        function() {
            res.json(medias);
        }
    );
};

router.post('/auth/instagram', function(req, res) {
    var modeConfig = getDeployMode(webConfig, req);

    var instagramAuthUrl = 'https://api.instagram.com/oauth/authorize/';
    var params = {
        client_id: modeConfig.instagram_api_key,
        redirect_uri: modeConfig.instagram_callback_url,
        response_type: 'code',
    };

    request.post({
        url: instagramAuthUrl,
        form: params
    }, function(err, httpResponse, body) {
        if (err || typeof httpResponse.headers.location === 'undefined') {
            err = err || body.meta.error_message;
            logger.error('Authorize with instagram error: ' + err.stack || err);
            util.serverErrorHandler(502, res);
            return;
        }

        // frontend needs to store remember_me property
        var responseUrl = instagramAuthUrl;
        var query = '?client_id=' + params.client_id + '&redirect_uri=' + params.redirect_uri + '&response_type=code';
        var encoded = encodeURIComponent(query);
        logger.debug('response message ' + query);
        res.json({
            url: responseUrl + encoded
        });
    });
});

router.get('/auth/instagram/callback', function(req, res) {
    var modeConfig = getDeployMode(webConfig, req);

    var accessTokenUrl = 'https://api.instagram.com/oauth/access_token';
    var code = url.parse(req.url, true).query.code;

    var params = {
        client_id: modeConfig.instagram_api_key,
        redirect_uri: modeConfig.instagram_callback_url,
        client_secret: modeConfig.instagram_api_secret,
        code: code,
        grant_type: 'authorization_code'
    };

    var getAccessTokenTimer = new Timer();
    request.post({
        url: accessTokenUrl,
        form: params,
        json: true
    }, function(err, httpResponse, body) {
        logger.debug('Instagram post access token api request time: ' + getAccessTokenTimer.elapseTime() + ' ms');

        if (err) {
            err = err || body.meta.error_message;
            logger.error('Instagram callback error: ' + err.stack || err);
            util.serverErrorHandler(err, httpResponse.statusCode, res);
            return;
        }

        if(body.user) {
            body.user.provider = 'instagram';
            logger.debug('[callback] checking user in DB');
            // insert if not exist
            userDao.exist(body, req, res);
        }
    });
});

router.get('/users/self', util.ensureAuthenticated, function(req, res) {
    userDao.queryById(req, res);
});

router.get('/users/self/media/liked', util.ensureAuthenticated, function(req, res) {
    var queryString = url.parse(req.url, true).query;

    var params = {
        access_token: req.user.access_token
    };
    if (queryString.count) {
        params.count = queryString.count;
    }
    if (queryString.max_id) {
        params.max_like_id = queryString.max_id;
    }

    var selfMediaLiked = 'https://api.instagram.com/v1/users/self/media/liked';
    var selfLikedTimer = new Timer();

    request.get({
        url: selfMediaLiked,
        qs: params,
        json: true
    }, function(err, httpResponse, body) {
        logger.debug('Instagram get self media liked api request time: ' + selfLikedTimer.elapseTime() + ' ms');

        if (err || httpResponse.statusCode !== 200) {
            err = err || body.meta.error_message;
            logger.error('Get self liked medias error: ' + err.stack || err);
            util.serverErrorHandler(httpResponse.statusCode, res);
            return;
        }

        querySetFavPhotosIfNeeded(req.user.id, body.data, res);
    });
});

router.get('/users/self/favorite', util.ensureAuthenticated, function(req, res) {
    var queryString = url.parse(req.url, true).query;
    var begin = parseInt(queryString.begin) || 0;

    favDao.queryAllByPage(req, res, begin);
});

router.post('/users/self/remember', util.ensureAuthenticated, function(req, res) {
    authDao.connectDB()
        .then(function(conn) {
            return authDao.updateRememberMe(conn, req.user.id, req.headers.authorization)
                .fin(function() {
                    conn.release();
                });
        }).then(
            function() {
                res.status(200).end();
            },
            function(err) {
                logger.error('[updateRememberMe] error: ' + err);
                util.serverErrorHandler(503, res);
                return;
            }
        );
});

router.get('/users/search', util.ensureAuthenticated, function(req, res) {
    var queryString = url.parse(req.url, true).query;

    var params = {
        access_token: req.user.access_token
    };

    logger.debug('search query: ' + queryString.q);
    if (queryString.q || queryString.q === '') {
        params.q = queryString.q;
    } else {
        res.status(200).end();
        return;
    }

    var searchUserUrl = 'https://api.instagram.com/v1/users/search';
    var searchUserTimer = new Timer();
    request.get({
        url: searchUserUrl,
        qs: params,
        json: true

    }, function(err, httpResponse, body) {
        logger.debug('Instagram search user api request time:' + searchUserTimer.elapseTime() + ' ms');

        if (err || httpResponse.statusCode !== 200) {
            err = err || body.meta.error_message;
            logger.error('Search User ' + queryString.q + ' error: ' + err.stack || err);
            util.serverErrorHandler(httpResponse.statusCode, res);
        } else {
            res.json(body.data);
        }
    });
});

router.get('/users/:user_id', util.ensureAuthenticated, function(req, res) {
    var userDetailUrl = 'https://api.instagram.com/v1/users/' + req.params.user_id;

    var params = {
        access_token: req.user.access_token
    };
    var selfDetailTimer = new Timer();
    request.get({
        url: userDetailUrl,
        qs: params,
        json: true
    }, function(err, httpResponse, body) {
        logger.debug('Instagram get user detail api request time: ' + selfDetailTimer.elapseTime() + ' ms');

        if (err || httpResponse.statusCode !== 200) {
            if (httpResponse.statusCode === 404) {
                err = 'Page Not Found';
            }
            err = err || body.meta.error_message;
            logger.error('Get user detail error: ' + err.stack || err);
            util.serverErrorHandler(httpResponse.statusCode, res, err);
        } else {
            res.json(body.data);
        }
    });
});

router.get('/feed/self', util.ensureAuthenticated, function(req, res) {
    var queryString = url.parse(req.url, true).query;

    var params = {
        access_token: req.user.access_token
    };
    if (queryString.count) {
        params.count = queryString.count;
    }
    if (queryString.max_id) {
        params.max_id = queryString.max_id;
    }
    if (queryString.min_id) {
        params.min_id = queryString.min_id;
    }

    var selfPhotoFeedUrl = 'https://api.instagram.com/v1/users/self/feed';
    var selfFeedTimer = new Timer();

    request.get({
        url: selfPhotoFeedUrl,
        qs: params,
        json: true
    }, function(err, httpResponse, body) {
        logger.debug('Instagram get self feed api request time: ' + selfFeedTimer.elapseTime() + ' ms');

        if (err || httpResponse.statusCode !== 200) {
            err = err || body.meta.error_message;
            logger.error('Get self feed error: ' + err.stack || err);
            util.serverErrorHandler(httpResponse.statusCode, res);
        } else {
            querySetFavPhotosIfNeeded(req.user.id, body.data, res);
        }
    });
});

router.post('/users/self/logout', util.ensureAuthenticated, function(req, res) {
    var token = req.headers.authorization;
    authDao.connectDB()
        .then(function(conn) {
            return authDao.updateLogout(conn, req.user.id, token)
                .fin(function() {
                    conn.release();
                });
        }).then(
            function() {
                logger.debug('user ' + req.user.id + ' logout successful');
                res.cookie('auth_token', '');
                res.status(200).end();
            },
            function(err) {
                logger.debug('user ' + req.user.id + ' logout unsuccessful: ' + err);
                util.serverErrorHandler(503, res);
            }
        );
});

router.get('/media/:media_id/comments', util.ensureAuthenticated, function(req, res) {
    var commentsUrl = 'https://api.instagram.com/v1/media/' + req.params.media_id + '/comments';
    var params = {
        access_token: req.user.access_token
    };

    var mediaCommentsTimer = new Timer();
    request.get({
        url: commentsUrl,
        qs: params,
        json: true
    }, function(err, httpResponse, body) {
        logger.debug('Instagram get media comments api request time: ' + mediaCommentsTimer.elapseTime() + ' ms');

        if (err || httpResponse.statusCode !== 200) {
            err = err || body.meta.error_message;
            logger.error('Get ' + req.params.media_id + ' comments error: ' + err.stack || err);
            util.serverErrorHandler(httpResponse.statusCode, res);
        } else {
            res.json(body.data);
        }
    });
});

router.post('/media/:media_id/favorite', util.ensureAuthenticated, function(req, res) {
    var mediaInfoUrl = 'https://api.instagram.com/v1/media/' + req.params.media_id;
    var params = {
        access_token: req.user.access_token
    };

    var mediaTimer = new Timer();
    request.get({
        url: mediaInfoUrl,
        qs: params,
        json: true
    }, function(err, httpResponse, body) {
        logger.debug('Instagram get media info api request time: ' + mediaTimer.elapseTime() + ' ms');

        if (err || httpResponse.statusCode !== 200) {
            err = err || body.meta.error_message;
            logger.error('Get ' + req.params.media_id + ' info error: ' + err.stack || err);
            util.serverErrorHandler(httpResponse.statusCode, res);
        } else {
            favDao.add(body, req, res);
        }
    });
});

router.post('/media/:media_id/unfavorite', util.ensureAuthenticated, function(req, res) {
    favDao.del(req, res);
});

router.get('/media/:media_id/favorite', util.ensureAuthenticated, function(req, res) {
    favDao.exist(req, res);
});

router.get('/media/:media_id', util.ensureAuthenticated, function(req, res) {
    var mediaInfoUrl = 'https://api.instagram.com/v1/media/' + req.params.media_id;
    var params = {
        access_token: req.user.access_token
    };

    var mediaTimer = new Timer();
    request.get({
        url: mediaInfoUrl,
        qs: params,
        json: true
    }, function(err, httpResponse, body) {
        logger.debug('Instagram get media info api request time: ' + mediaTimer.elapseTime() + ' ms');

        if (err || httpResponse.statusCode !== 200) {
            err = err || body.meta.error_message;
            logger.error('Get ' + req.params.media_id + ' info error: ' + err.stack || err);
            util.serverErrorHandler(httpResponse.statusCode, res);
        } else {
            querySetFavPhotosIfNeeded(req.user.id, [body.data], res);
        }
    });
});

router.get('/users/:user_id/media/recent', util.ensureAuthenticated, function(req, res) {
    var recentMediaUrl = 'https://api.instagram.com/v1/users/' + req.params.user_id + '/media/recent/';
    var queryString = url.parse(req.url, true).query;

    var params = {
        access_token: req.user.access_token
    };
    if (queryString.count) {
        params.count = queryString.count;
    }
    if (queryString.max_id) {
        params.max_id = queryString.max_id;
    }
    if (queryString.min_id) {
        params.min_id = queryString.min_id;
    }
    if( queryString.max_timestamp) {
        params.max_timestamp = queryString.max_timestamp;
    }
    if( queryString.min_timestamp) {
        params.min_timestamp = queryString.min_timestamp;
    }

    var recentMediaTimer = new Timer();

    request.get({
        url: recentMediaUrl,
        qs: params,
        json: true
    }, function(err, httpResponse, body) {
        logger.debug('Instagram get recent media api request time: ' + recentMediaTimer.elapseTime() + ' ms');

        if (err || httpResponse.statusCode !== 200) {
            err = err || body.meta.error_message;
            logger.error('Get self feed error: ' + err.stack || err);
            util.serverErrorHandler(httpResponse.statusCode, res);
        } else {
            res.json(body.data);
        }
    });
});

router.get('/search',util.ensureAuthenticated, function(req, res) {
    var transport = thrift.TBufferedTransport();
    var protocol = thrift.TBinaryProtocol();
    var queryString = url.parse(req.url, true).query;

    var connection = thrift.createConnection("localhost", 9000, {
        transport : transport,
        protocol : protocol
    });

    connection.on('error', function(err) {
        console.error(err);
    });

    var client = thrift.createClient(search_svc, connection);

    var searchTimer = new Timer();
    client.search(req.user.access_token, queryString, function(err, result) {
        logger.debug('Search Server search request time: ' + searchTimer.elapseTime() + ' ms');
        connection.end();
        if(err) {
            logger.error('[search] Error: ' + err);
        }
        else {
            res.json(result);
        }
    });
});

// post comments and likes are not allowed by instagram
// router.post('/media/:media_id/comments', util.ensureAuthenticated, function(req, res) {
//  var commentsUrl = 'https://api.instagram.com/v1/media/'+ req.params.media_id + '/comments';

//  var params = {
//      text : req.body.text
//  };

//  request.post({ url: commentsUrl, form: params, json: true}, function(err, httpResponse, body) {
//      if (err || httpResponse.statusCode !== 200) {
//          err = err || body.meta.error_message;
//          logger.error('Post ' + req.params.media_id + ' comments error: ' + err);
//          util.serverErrorHandler(httpResponse.statusCode, res);
//          return;
//      }

//      res.status(200).end();
//  });
// });

// router.post('/media/:media_id/likes', util.ensureAuthenticated, function(req, res) {
//  var likesUrl = 'https://api.instagram.com/v1/media/'+ req.params.media_id + '/likes';

//  var params = { access_token: req.body.access_token };
//  request.post({ url: likesUrl, form: params}, function(err, httpResponse, body) {
//      if (err || httpResponse.statusCode !== 200) {
//          err = err || body.meta.error_message;
//          logger.error('Post ' + req.params.media_id + ' like error: ' + err);
//          util.serverErrorHandler(httpResponse.statusCode, res);
//          return;
//      }

//      res.status(200).end();
//  });
// });

module.exports = router;
