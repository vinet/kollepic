var dbm = global.dbm || require('db-migrate');
var type = dbm.dataType;

exports.up = function(db, callback) {
    db.addColumn('users', 'email', {
        type: 'string',
        length: '40'
    }, callback);
};

exports.down = function(db, callback) {
    callback();
};
