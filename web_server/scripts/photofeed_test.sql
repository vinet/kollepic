CREATE DATABASE if not exists photofeed_test;
USE photofeed_test;

CREATE TABLE if not exists users (
	id INT not null,
	username VARCHAR(30) not null,
	profile_picture VARCHAR(2083),
	full_name VARCHAR(30),
	provider VARCHAR(10) not null,
	CONSTRAINT pk_userID PRIMARY KEY (id),
	email VARCHAR(60) DEFAULT 'unknown'
);

CREATE TABLE if not exists fav_photos (
	userId INT not null,
	mediaId VARCHAR(30) not null,
	fav_timestamp TIMESTAMP not null,
	low_resolution VARCHAR(2083),
	thumbnail VARCHAR(2083),
	standard_resolution VARCHAR(2083),
	type VARCHAR(5),
	video_standard_resolution VARCHAR(2083),
	create_timestamp TIMESTAMP not null,
	create_userId INT not null,
	create_username VARCHAR(20) not null,
	rating INT DEFAULT '-1',
	CONSTRAINT pk_userId_mediaId PRIMARY KEY (userId, mediaId)
);
ALTER TABLE fav_photos ADD INDEX (fav_timestamp);

CREATE TABLE if not exists auth_users (
	auth_token VARCHAR(160) not null,
	id INT not null,
	access_token VARCHAR(60) not null,
	last_auth_ts TIMESTAMP not null,
	first_auth_ts TIMESTAMP not null,
	remember_me BOOLEAN DEFAULT FALSE not null,
	logout BOOLEAN DEFAULT FALSE,
	user_agent VARCHAR(40) DEFAULT 'unknown',
	CONSTRAINT pk_authUserId PRIMARY KEY (auth_token)
);
ALTER TABLE auth_users ADD INDEX (id);
