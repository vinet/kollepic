var pool = require('../conf/database');
var logger = require('../conf/log');
var util = require('../util/util');
var authDao = require('./authDao');
var favoritedao = require('./favoriteDao');

var sql_statements = {
    insert: 'insert into users(id, username, profile_picture, full_name, provider) values(?, ?, ? ,?, ?)',
    update: 'update users set username=?, profile_picture=?, full_name=?, provider=? where id=?',
    queryById: 'select * from users where id=?',
    queryAll: 'select * from users',
    exist: 'select 1 from users where id=?'
};

var setTokenThenRedirect = function(auth_token, req, res) {
    res.cookie('auth_token', auth_token);
    res.redirect('/');
};

var insert = function(body, req, res) {
    pool.getConnection(function(err, conn) {
        if (err) {
            logger.error('Pool getConnection: ' + err.stack);
            util.serverErrorHandler(503, res);
            return;
        }

        var user = body.user;
        var userCols = [user.id, user.username, user.profile_picture, user.full_name, user.provider];

        try {
            // update User table
            conn.beginTransaction(function(err) {
                if (err) {
                    logger.error('DB beginTransaction error: ' + err.stack);
                } else {
                    conn.query(sql_statements.insert, userCols, function(err) {
                        if (err) {
                            logger.error('DB [insert] against table [users] error: ' + err.stack);
                            conn.rollback(function() {
                                throw err;
                            });
                        } else {
                            var auth_token = util.generateAuthToken(user.id, req.app.get('secureToken'));
                            // update auth_users
                            authDao.insert(conn, user.id, body.access_token, auth_token).then(
                                function() {
                                    conn.commit(function(err) {
                                        if (err) {
                                            conn.rollback(function() {
                                                logger.error('DB transaction commit error: ' + err.stack);
                                                throw err;
                                            });
                                        }
                                        conn.release();
                                        setTokenThenRedirect(auth_token, req, res);
                                    });
                                },
                                function(err) {
                                    conn.rollback(function() {
                                        logger.error('DB [insert] against table [auth_users] error: ' + err.stack);
                                        throw err;
                                    });
                                }
                            );
                        }
                    });
                }
            });
        } catch (err) {
            conn.release();
            util.serverErrorHandler(503, res);
        }
    });
};

var exist = function(body, req, res) {
    // The user login from the first login page. Refresh last_auth_ts directly
    pool.getConnection(function(err, conn) {
        if (err) {
            logger.error('Pool getConnection: ' + err.stack);
            util.serverErrorHandler(503, res);
            return;
        }

        var userId = body.user.id;
        conn.query(sql_statements.exist, [userId], function(err, result) {
            conn.release();

            if (err) {
                logger.error('DB [exist] against table [users] error: ' + err.stack);
                util.serverErrorHandler(503, res);
                return;
            }


            // user exist
            if (result.length > 0) {
                logger.debug('DB [exist] user ' + userId + ' exist');

                // generate auth token
                var auth_token = util.generateAuthToken(userId, req.app.get('secureToken'));

                // check if user is already login with same auth_token, insert if not exist
                authDao.connectDB()
                    .then(function(conn) {
                        return authDao.insert(conn, userId, body.access_token, auth_token)
                            .fin(function() {
                                conn.release();
                            });
                    }).then(
                        function() {
                            setTokenThenRedirect(auth_token, req, res);
                        },
                        function(err) {
                            logger.error('DB [exist] against auth_users error' + err.stack);
                            util.serverErrorHandler(503, res);
                        }
                    );
            } else { // insert to db
                insert(body, req, res);
            }
        });
    });
};

var queryById = function(req, res) {
    pool.getConnection(function(err, conn) {
        if (err) {
            logger.error('Pool getConnection: ' + err.stack);
            util.serverErrorHandler(503, res);
            return;
        }

        var userId = req.user.id;
        conn.query(sql_statements.queryById, [userId], function(err, result) {

            if (err) {
                conn.release();
                logger.error('DB [queryById] against table [users] error: ' + err.stack);
                util.serverErrorHandler(503, res);
                return;
            }

            favoritedao.getFavCountById(conn, userId)
                .then(function(n) {
                        result[0].n_fav_photos = n[0].n_fav_photos;
                        return authDao.getLastLogin(conn, userId)
                        .fin(function(){
                            conn.release();
                        });
                    })
                .then(function(rt) {
                    result[0].last_login_ts = rt[0].last_auth_ts;
                    res.json(result);
                },
                function(err) {
                    logger.error('[queryById] :' + err.stack);
                    util.serverErrorHandler(503, res);
                });

        });
    });
};

module.exports = {
    exist: exist,
    queryById: queryById
};
