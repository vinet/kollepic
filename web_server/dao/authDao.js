var pool = require('../conf/database');
var logger = require('../conf/log');
var moment = require('moment');
var Q = require('q');

var sql_statements = {
    insert: 'insert into auth_users (id, access_token, auth_token, last_auth_ts, first_auth_ts) values(?, ?, ?, ?, ?)',
    update_rememberMe: 'update auth_users set remember_me=1 where auth_token=?',
    queryByAuthToken: 'select * from auth_users where auth_token=?',
    exist: 'select 1 from auth_users where auth_token=?',
    update_logout: 'update auth_users set logout=1 where auth_token=?',
    update_login: 'update auth_users set logout=0, last_auth_ts=? where auth_token=?',
    delete_logoutUser: 'delete from auth_users where auth_token=?',
    get_lastLoginById: 'select last_auth_ts from auth_users where id=? order by last_auth_ts desc',
};

var connectDB = function() {
    var deferred = Q.defer();
    pool.getConnection(function(err, conn) {
        if (err) {
            logger.error('Pool getConnection: ' + err.stack);
            deferred.reject(err);
        } else {
            deferred.resolve(conn);
        }
    });
    return deferred.promise;
};

var updateLogout = function(conn, userId, auth_token) {
    var deferred = Q.defer();
    // should remove the row directly
    conn.query(sql_statements.delete_logoutUser, [auth_token], function(err) {
        if (err) {
            logger.error('DB [updateLogout] against table [auth_users] error: ' + err.stack);
            deferred.reject(err);
        } else {
            logger.debug('DB [updateLogout] user ' + userId + 'logout successful');
            deferred.resolve();
        }
    });
    return deferred.promise;
};

var updateLogin = function(conn, user_id, auth_token) {
    var deferred = Q.defer();
    // update logout status
    var now = moment().format('YYYY-MM-DD H:mm:ss z');
    conn.query(sql_statements.update_login, [now, auth_token], function(err) {
        if (err) {
            logger.error('DB [updateLogin] against table [auth_user] error: ' + err.stack);
            deferred.reject(err);
        } else {
            logger.debug('DB [updateLogin] User ' + user_id + ' login has updated');
            deferred.resolve();
        }
    });
    return deferred.promise;
};

var queryByAuthToken = function(conn, userId, auth_token) {

    var deferred = Q.defer();
    conn.query(sql_statements.queryByAuthToken, [auth_token], function(err, result) {

        if (err) {
            logger.error('DB [queryByAuthToken] against table [auth_users] error: ' + err.stack);
            deferred.reject(err);
        } else {
            // user doesn't exist
            if (result.length === 0) {
                logger.warn('DB [queryByAuthToken] user ' + userId + ' does not exist');
                deferred.reject(new Error('Table [auth_users] Cannot find user: ' + userId));
            } else {
                logger.info('[queryByAuthToken] authUser exist: ' + userId);
                checkLoginStatus(conn, result[0]).then(
                    function() {
                        deferred.resolve(result[0]);
                    },
                    function(err) {
                        logger.error('[queryByAuthToken] checkLoginStatus: ' + err.stack);
                        deferred.reject(err);
                    }
                );
            }
        }
    });
    return deferred.promise;
};


// reject if user is already logout or expired, accept and update otherwise
var checkLoginStatus = function(conn, user) {
    var deferred = Q.defer();
    // user is already logout, reject
    if (user.logout === 1) {
        logger.warn('user ' + user.id + ' already logout, access denied');
        deferred.reject(new Error('User ' + user.id + ' already logout'));
    }
    // authenticate user if remember me is selected or last login time is in 30 mins and user is not logout
    else if (user.remember_me === 1 || (moment().unix() - moment(user.last_auth_ts).unix()) / 60 < 30) {
        logger.debug('user ' + user.id + ' updating last access timestamp...');
        updateLogin(conn, user.id, user.auth_token).then(
            function() {
                deferred.resolve();
            },
            function(err) {
                deferred.reject(err);
            }
        );
    } else {
        logger.debug('[checkLoginStatus] session expired now: ' + moment().unix() + ' lastAuthTs: ' + moment(user.last_auth_ts).unix());
        // login expired, logout
        updateLogout(conn, [user.id, user.auth_token]).then(
            function() {
                deferred.reject(new Error('User ' + user.id + ' login expires: '));
            },
            function(err) {
                deferred.reject(new Error('User ' + user.id + ' update logout status failed: ' + err));
            }
        );
    }
    return deferred.promise;
};

var updateRememberMe = function(conn, userId, auth_token) {

    var deferred = Q.defer();
    conn.query(sql_statements.update_rememberMe, [auth_token], function(err) {
        if (err) {
            logger.error('DB [updateRememberMe] against table [auth_users] error: ' + err.stack);
            deferred.reject(err);
        } else {
            logger.debug('User ' + userId + ' updates remember_me to true');
            deferred.resolve();
        }
    });
    return deferred.promise;
};

// insert method is called by userDao.add. conn is released in userDao.add
// pass database conection argument conn for transaction implementation.
var insert = function(conn, userId, access_token, auth_token) {

    var deferred = Q.defer();
    var now = moment().format('YYYY-MM-DD H:mm:ss z');
    conn.query(sql_statements.insert, [userId, access_token, auth_token, now, now], function(err) {
        if (err) {
            logger.error('DB [insert] against table [auth_users] error: ' + err.stack);
            deferred.reject(err);
        } else {
            logger.debug('DB [insert] User ' + userId + ' information successfully');
            deferred.resolve();
        }
    });
    return deferred.promise;
};

var exist = function(conn, userId, access_token, auth_token) {

    var deferred = Q.defer();
    // The user login from the first login page. Refresh last_auth_ts directly
    // if exist update last acctimestamp, otherwise, insert new
    conn.query(sql_statements.exist, [auth_token], function(err, result) {
        if (err) {
            conn.release();
            logger.error('DB [exist] against table [auth_users] error: ' + err.stack);
            deferred.reject(err);
        } else {
            var isExist = result.length > 0;
            insertIfNotExist(conn, isExist, userId, access_token, auth_token).then(
                function() {
                    deferred.resolve();
                },
                function(err) {
                    logger.error('[exist] authDao insert or update error:' + err.stack);
                    deferred.reject(err);
                }
            );
        }
    });
    return deferred.promise;
};


var insertIfNotExist = function(conn, isExist, userId, access_token, auth_token) {
    var deferred = Q.defer();
    // user exist
    if (isExist) {
        logger.debug('user ' + userId + ' exist');
        updateLogin(conn, userId, auth_token).then(
            function() {
                deferred.resolve();
            },
            function(err) {
                logger.error('DB [updateLogin] against table [auth_users] error: ' + err.stack);
                deferred.reject(err);
            }
        );
    }
    // user not exist->insert
    else {
        insert(conn, userId, access_token, auth_token).then(
            function() {
                deferred.resolve();
            },
            function(err) {
                logger.error('DB [insert] against table [auth_users] error: ' + err.stack);
                deferred.reject(err);
            }
        );
    }
    return deferred.promise;
};

var getLastLogin = function(conn, userId) {
	var deferred = Q.defer();
	conn.query(sql_statements.get_lastLoginById, [userId], function(err,ret){
		if(err) {
			logger.error('DB [getLastLogin] against table [auth_users] error: ' + err.stack);
			deferred.reject(err);
		}
		else {
			deferred.resolve(ret);
		}
	});
	return deferred.promise;
};

module.exports = {
    queryByAuthToken: queryByAuthToken,
    updateRememberMe: updateRememberMe,
    insert: insert,
    updateLogout: updateLogout,
    updateLogin: updateLogin,
    exist: exist,
    connectDB: connectDB,
    getLastLogin: getLastLogin,
};
