var pool = require('../conf/database');
var logger = require('../conf/log');
var util = require('../util/util');
var moment = require('moment');
var Q = require('q');

var sql_statements = {
    insert: 'insert into fav_photos(userId, mediaId, fav_timestamp, low_resolution, thumbnail, standard_resolution, type, video_standard_resolution, create_timestamp, create_userId, create_username) values(?, ?, ?, ?, ?, ?, ? ,?, ?, ? ,?)',
    queryAllByPage: 'select *, UNIX_TIMESTAMP(fav_timestamp) AS fav_timestamp_unix from fav_photos where userId=? order by ?? DESC limit ?, ?',
    exist: 'select 1 from fav_photos where userId=? and mediaId=?',
    queryByUserIdMultiMediaIds: 'select mediaId from fav_photos where userId=? and mediaId in (?)',
    del: 'delete from fav_photos where userId=? and mediaId=?',
    countFavById: 'select COUNT(*) as n_fav_photos from fav_photos where userId=?',
};

var add = function(body, req, res) {
    pool.getConnection(function(err, conn) {
        if (err) {
            logger.error('Pool getConnection: ' + err.stack);
            util.serverErrorHandler(503, res);
            return;
        }

        var type = body.data.type;
        var video_standard_resolution = type === 'image' ? '' : body.data.videos.standard_resolution.url;

        var now = moment().format('YYYY-MM-DD H:mm:ss z');
        var create_timstamp = moment.unix(body.data.created_time).format('YYYY-MM-DD H:mm:ss z');
        var favCols = [req.user.id, req.params.media_id, now,
            body.data.images.low_resolution.url, body.data.images.thumbnail.url,
            body.data.images.standard_resolution.url, type, video_standard_resolution,
            create_timstamp, body.data.user.id, body.data.user.username
        ];

        conn.query(sql_statements.insert, favCols, function(err) {
            conn.release();

            if (err) {
                logger.error('DB [insert] against table [fav_photos] error: ' + err.stack);
                util.serverErrorHandler(503, res);
            } else {
                res.status(200).end();
            }
        });
    });
};

var queryAllByPage = function(req, res, begin) {
    pool.getConnection(function(err, conn) {
        var queryString = req.query;
        if (err) {
            logger.error('Pool getConnection: ' + err.stack);
            util.serverErrorHandler(503, res);
            return;
        }

        var end = begin + 12; // default pagination count 12
        var sortBy = '';
        queryString.sort = queryString.sort || 'favorited_at';

        switch (queryString.sort) {
            case 'created_at':
                sortBy = 'create_timestamp';
                break;
            case 'favorited_at':
                sortBy = 'fav_timestamp';
                break;
            case 'create_username':
                sortBy = 'create_username';
                break;
            default:
                sortBy = 'fav_timestamp';
                break;
        }

        var favConditions = [req.user.id, sortBy, begin, end];

        conn.query(sql_statements.queryAllByPage, favConditions, function(err, result) {
            conn.release();

            if (err) {
                logger.error('DB [queryAllByPage] against table [fav_photos] error: ' + err.stack);
                util.serverErrorHandler(503, res);
                return;
            }

            result.next_page_begin = end + 1;
            res.json(result);
        });
    });
};

var exist = function(req, res) {
    var userId = req.user.id;
    var mediaId = req.params.media_id;
    pool.getConnection(function(err, conn) {
        if (err) {
            logger.error('Pool getConnection: ' + err.stack);
            util.serverErrorHandler(503, res);
            return;
        }

        conn.query(sql_statements.exist, [userId, mediaId], function(err, result) {
            conn.release();

            if (err) {
                logger.error('DB [exist] against table [fav_photos] error: ' + err.stack);
                util.serverErrorHandler(503, res);
                return;
            }

            if (result.length > 0) {
                res.json({
                    result: true
                });
            } else {
                res.json({
                    result: false
                });
            }
        });
    });
};

var queryByUserIdMultiMediaIds = function(userId, mediaIds) {
    var deferred = Q.defer();
    pool.getConnection(function(err, conn) {
        if (err) {
            logger.error('Pool getConnection: ' + err.stack);
            deferred.reject(err);
        } else {
            var condStatement = [userId, mediaIds];

            conn.query(sql_statements.queryByUserIdMultiMediaIds, condStatement, function(err, result) {
                conn.release();

                if (err) {
                    logger.error('DB [queryByUserIdMultiMediaIds] against table [fav_photos] error: ' + err.stack);
                    deferred.reject(err);
                } else {
                    var favMediaIds = [];
                    result.forEach(function(r) {
                        favMediaIds.push(r.mediaId);
                    });
                    deferred.resolve(favMediaIds);
                }
            });
        }
    });
    return deferred.promise;
};

var del = function(req, res) {
    pool.getConnection(function(err, conn) {
        if (err) {
            logger.error('Pool getConnection: ' + err.stack);
            util.serverErrorHandler(503, res);
            return;
        }

        var favConditions = [req.user.id, req.params.media_id];

        conn.query(sql_statements.del, favConditions, function(err) {
            conn.release();

            if (err) {
                logger.error('DB [delete] against table [fav_photos] error: ' + err.stack);
                util.serverErrorHandler(204, res);
                return;
            }

            res.status(200).end();
        });
    });
};

var getFavCountById = function(conn, userId) {
    var deferred = Q.defer();
    conn.query(sql_statements.countFavById, [userId], function(err, ret) {
        if (err) {
            logger.err('DB [getFavCountById] against table [fav_photos] error: ' + err.stack);
            deferred.reject(err);
        } else {
            deferred.resolve(ret);
        }
    });

    return deferred.promise;

};

module.exports = {
    add: add,
    queryAllByPage: queryAllByPage,
    exist: exist,
    queryByUserIdMultiMediaIds: queryByUserIdMultiMediaIds,
    del: del,
    getFavCountById: getFavCountById,
};
