#! /usr/local/bin/node

var program = require('commander');
var moment = require('moment');
var mysql = require('mysql');
var winston = require('winston');

var loggerConfig = {
    transports: [
        new winston.transports.Console({
            level: 'info',
            handleExceptions: true,
            json: false,
            colorize: true
        })
    ],
    exitOnError: false
};

var logger = new winston.Logger(loggerConfig);

program
    .option('-t, --test', 'Test mode')
    .option('-d, --dev', 'Dev mode')
    .parse(process.argv);

var deleteObsoleteRows = function (conn) {
    // delete obsolete rows if this auth user id is idle for 
    // more than 1 hour and remember me is not selected or this id is logout
    var delete_obsolete_rows = 'delete from auth_users where (NOW() > last_auth_ts + Interval 1 hour and remember_me=0) or logout=1';

    conn.query(delete_obsolete_rows, function (err) {
        conn.end();
        
        if (err) {
            logger.error('DB [delete_obsolete_rows] against table [auth_user] error: ' + err.stack);
        } else {
            logger.info('DB [delete_obsolete_rows] against table [auth_user] is done');
        }
    });
};

var database_name = program.test? 'photofeed_test': 'photofeed';

var conn = mysql.createConnection({
    host: '127.0.0.1',
    user: 'root',
    password: '',
    database: database_name,
    port: 3306
});

conn.connect(function (err) {
    if (!err) {
        logger.info("Database is connected ...");
    } else {
        logger.error("Error connecting database ...");
    }
});

deleteObsoleteRows(conn);