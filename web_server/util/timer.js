var moment = require('moment');

var Timer = function Timer() {
	this.start_time = moment().format('x');
};

Timer.prototype.elapseTime = function() {
	return moment().format('x') - this.start_time;
};

module.exports = Timer;