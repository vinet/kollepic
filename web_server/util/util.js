var jwt = require('jsonwebtoken');
var moment = require('moment');
var logger = require('../conf/log');
var authDao = require('../dao/authDao');

var ensureAuthenticated = function(req, res, next) {
    var token = req.headers.authorization;
    if (token) {
        jwt.verify(token, req.app.get('secureToken'), function(err, payload) {
            if (err) {
            	res.cookie('auth_token', '');
                logger.error('Token verify error: ' + err.stack);
                serverErrorHandler(403, res, 'Fail to authenticate user');
            } else {
                req.user = {
					id: payload.iss,
				};

                var userId = req.user.id;
                authDao.connectDB()
                .then(function(conn){
                    return authDao.queryByAuthToken(conn, userId, token)
                    .fin(function(){
                        conn.release();
                    });
                }).then(
					function(ret){
                        // this should never happen, will be delete after review
						if (ret.auth_token !== token) {
							logger.warn('Table [auth_dao] error: auth_token not match');
                            logger.debug('ret.auth_token: ' + ret.auth_token);
                            logger.debug('ret.token: ' + token);
							logger.warn('User ' + userId + ' authenticates fail');
							res.cookie('auth_token', '');
							serverErrorHandler(403, res, 'Fail to authenticates user');
						}
						else {
							req.user.access_token = ret.access_token;
							next();
						}
					},
					function(err){
                        logger.warn('Table [auth_dao] error: ' + err);
                		logger.warn('User ' + userId + ' authenticates fail');
                		res.cookie('auth_token', '');
                    	serverErrorHandler(403, res, 'Fail to authenticates user');
					}
				);
            }
        });
    } else {
        res.cookie('auth_token', '');
        serverErrorHandler(403, res, 'No authentication token provided');
    }
};

var generateAuthToken = function(userId, secureToken) {
    var payload = {
        iat: moment().unix(),
        iss: userId
    };
    var token = jwt.sign(payload, secureToken);

    logger.info('Authorization token: ' + token);
    return token;
};

var serverErrorHandler = function(statusCode, res, message) {
	if (typeof message === 'undefined') {
 	   res.status(statusCode).end();
	} else {
		res.status(statusCode).send({
			message: message
		});
	}
};

module.exports = {
    ensureAuthenticated: ensureAuthenticated,
    generateAuthToken: generateAuthToken,
    serverErrorHandler: serverErrorHandler
};
