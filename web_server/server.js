var express = require('express');
var fs = require('fs');
var mime = require('mime-types');
var compress = require('compression');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var webConfig = require('./conf/web');
var router = require('./routes/api');
var logger = require('./conf/log');

var app = express();

var node_type = process.env.NODE_TYPE || 'development';

app.set('secureToken', webConfig.secret);

var format = ':remote-addr - :remote-user [:date[clf]] :method :url HTTP/:http-version :status :response-time ms :res[content-length]';
app.use(require('morgan')(format, {
    'stream': logger.stream
}));

app.use(compress());
app.use(cookieParser());

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({
    extended: false
}));

// parse application/json
app.use(bodyParser.json());

app.use('/api', router);

var ROOT_DIR = __dirname + '/../client/release';

if (node_type === 'production') {
    app.use(express.static(ROOT_DIR, {
        maxAge: '1y',
        setHeaders: function (res, path) {
            if (mime.lookup(path) === 'text/html') {    // No cache for html
                res.setHeader('Cache-Control', 'private, no-cache, no-store, must-revalidate');
                res.setHeader('Expires', '-1');
                res.setHeader('Pragma', 'no-cache');
            }
        }
    }));
} else {
    ROOT_DIR = __dirname + '/../client/src';
    app.use(express.static(ROOT_DIR));
}

ROOT_DIR = fs.realpathSync(ROOT_DIR);
if (!fs.existsSync(ROOT_DIR)) {
    logger.error('Cannot find working directory: ' + ROOT_DIR);
    process.exit(1);
}

// Fix AnuglarJS path resolving issue. Force all none api get request to index page
app.get('/*', function(req, res) {
    res.sendFile(ROOT_DIR + '/index.html');  
});

app.use(function (req, res) {
    res.status(404);
    res.send({
        error: req.method + ' ' + req.url + ' not found'
    });
});

process.on('uncaughtException', function (err) {
    logger.error('UncaughtException: ' + err.stack);
});

var port = process.env.PORT || 3000;
app.listen(port, function () {
    app.set('node_type', node_type);

    logger.info('Listening on port ' + port);
    logger.info('Web server running type: ' + node_type);
    logger.info('Serving static files from: ' + ROOT_DIR);
});
