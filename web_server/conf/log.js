var winston = require('winston');

var loggerConfig = {
    transports: [
        new winston.transports.File({
            level: 'info',
            filename: './logs/server.log',
            handleExceptions: true,
            json: false,
            maxsize: 10485760, //10MB
            maxFiles: 5,
            colorize: false
        })
    ],
    exitOnError: false
};

var node_type = process.env.NODE_TYPE || 'development';
if (node_type === 'development') {
    loggerConfig.transports.push(
        new winston.transports.Console({
            level: 'debug',
            handleExceptions: true,
            json: false,
            colorize: true
    }));
}

var logger = new winston.Logger(loggerConfig);

var accessLogger = new winston.Logger({
    transports: [
        new winston.transports.File({
            level: 'info',
            filename: './logs/access.log',
            handleExceptions: false,
            json: false,
            maxsize: 10485760, //10MB
            maxFiles: 5,
            colorize: false
        })
    ],
    exitOnError: false
});

module.exports = logger;
module.exports.stream = {
    write: function(message) {
        accessLogger.info(message);
    }
};
