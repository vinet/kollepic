var mysql = require('mysql');
var logger = require('../conf/log');

var db_type = process.env.DB_TYPE || 'dev';
var database_name = db_type === 'dev'? 'photofeed': 'photofeed_test';

logger.info('running database type: ' + db_type + ' database name: ' + database_name);

var pool = mysql.createPool({
    host: '127.0.0.1',
    user: 'root',
    password: '',
    database: database_name,
    port: 3306
});

module.exports = pool;