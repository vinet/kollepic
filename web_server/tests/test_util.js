var jwt = require('jsonwebtoken');
var Q = require('q');
var moment = require('moment');
var authDao = require('../dao/authDao');

var decodeAuthToken = function(token, secureToken) {
	var deferred = Q.defer();
    jwt.verify(token, secureToken, function(err, payload) {
	    if (err) {
	        deferred.reject(err);
	    }
	    deferred.resolve(payload.iss);
	});
	return deferred.promise;
};

var updateLoginTime = function(conn, params){
	var deferred = Q.defer();
	var sql_statement = 'update auth_users set last_auth_ts=? where id=?';
	conn.query(sql_statement, params, function(err) {
		if(err) {
			console.log('[test] DB [updateLoginTime] update login time err: ' + err);
			deferred.reject(err);
		}
		else {
			deferred.resolve();
		}
	});
	return deferred.promise;
};


var increase_login_timestamp = function(userId) {
	var deferred = Q.defer();
	// one hour later from now;
    var now = moment(new Date() - 1000 * 40 * 60).format('YYYY-MM-DD H:mm:ss z');
    console.log('now: ' + now);
    authDao.connectDB()
    .then(function(conn){
    	return updateLoginTime(conn, [now, userId])
    	.fin(function(){
    		conn.release();
    	});
    })
    .then(
		function(){
			deferred.resolve();
		},
		function(err){
			deferred.reject(err);
		});
	return deferred.promise;
};

var force_login = function(conn, userId, access_token, auth_token) {
	var deferred = Q.defer();
	var sql_statement = {
		to_auth_users: 'INSERT INTO auth_users(id, access_token, auth_token, last_auth_ts, first_auth_ts) VALUES(?, ?,?, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)',
		to_users: 'insert into users(id, username, profile_picture, full_name, provider) values(?, ?, ? ,?, ?)',
	};
	conn.query(sql_statement.to_auth_users, [userId, access_token, auth_token], function(err) {
		if(err) {
			console.log('[test] DB [force_login] against table auth_users error: ' + err.stack);
			deferred.reject(err);
		}
		else {
			conn.query(sql_statement.to_users, [userId, 'test_user', null, 'test user', 'instagram'], function(err){
				if(err) {
					console.log('[test] DB [force_login] against table users error: ' + err.stack);
					deferred.reject(err);
				}
				else {
					console.log('[test] DB [force_login] insert successful');
					deferred.resolve();
				}
			});
		}
	});
	return deferred.promise;
};

var force_logout = function(conn, userId, access_token, auth_token) {
	var deferred = Q.defer();
	var sql_delete = {
		from_users: 'delete from users where id=?',
		from_auth_users: 'delete from auth_users where auth_token=?',
	};
	conn.query(sql_delete.from_users, [userId], function(err) {
		if(err) {
			console.log('DB [force_login] against users table error: ' + err.stack);
			deferred.reject(err);
		}
		else {
			conn.query(sql_delete.from_auth_users, [auth_token], function(err) {
				if(err) {
				console.log('DB [force_logout] against auth_users table error: ' + err.stack);
				deferred.reject(err);
				}
				else {
					deferred.resolve();
				}
			});
		}
	});
	return deferred.promise;
};
module.exports = {
    decodeAuthToken: decodeAuthToken,
	increase_login_timestamp: increase_login_timestamp,
	force_login: force_login,
	force_logout: force_logout,
};
