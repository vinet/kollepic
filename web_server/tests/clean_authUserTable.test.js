var should = require('should');
var mysql = require('mysql');
var moment = require('moment');

var insert_fake_authUsers = function (conn, id, last_auth_ts, remember_me, logout) {
	var auth_token = id.toString(16);
	var create_fake_authUsers = "insert into auth_users(id, access_token, auth_token, last_auth_ts, first_auth_ts, remember_me, logout) VALUES(?, 'abcde', ?, ?, CURRENT_TIMESTAMP, ?, ?)";

	conn.query(create_fake_authUsers, [id, auth_token, last_auth_ts, remember_me, logout], function (err) {
		if (err) {
			console.log('[test] DB [insert_fake_authUsers] err: ' + err.stack);
		}
	});
};

var query_authUsers_RowCount = function (conn, expectedCount) {
	var query_rowCount = 'select count(*) as count from auth_users';

	conn.query(query_rowCount, function (err, result) {
		if (err) {
			console.log('[test] DB [query_authUsers_RowCount] err: ' + err.stack);
		} else {
			result[0].count.should.equal(expectedCount);
		}
	});
};

var query_authUsers_ids = function(conn) {
	var query_rowCount = 'select id from auth_users';

	conn.query(query_rowCount, function (err, result) {
		if (err) {
			console.log('[test] DB [query_authUsers_ids] err: ' + err.stack);
		} else {
			result[0].id.should.equal(1);
			result[1].id.should.equal(4);
			result[2].id.should.equal(5);
		}
	});	
};

var delete_all_authUsers = function(conn) {
	var delete_authUser = 'delete from auth_users';
	
	conn.query(delete_authUser, function (err) {
		if (err) {
			console.log('[test] DB [delete_all_authUsers] err: ' + err.stack);
		}
	});
};

describe('photofeed clean authUserTable tests', function () {
	var conn = mysql.createConnection({
		host: '127.0.0.1',
		user: 'root',
		password: '',
		database: 'photofeed_test',
		port: 3306
	});

	before(function (done) {
		conn.connect(function (err) {
			if (err) {
				throw err;
			}
		});

		var id = 0;
		var two_hours_before = moment().subtract(2, 'hour').format("YYYY-MM-DD H:mm:ss z");
		var now = moment().format("YYYY-MM-DD H:mm:ss z");

		insert_fake_authUsers(conn, id++, two_hours_before, 0, 0);
		insert_fake_authUsers(conn, id++, two_hours_before, 1, 0);
		insert_fake_authUsers(conn, id++, two_hours_before, 0, 1);
		insert_fake_authUsers(conn, id++, two_hours_before, 1, 1);
		insert_fake_authUsers(conn, id++, now, 0, 0);
		insert_fake_authUsers(conn, id++, now, 1, 0);
		insert_fake_authUsers(conn, id++, now, 0, 1);
		insert_fake_authUsers(conn, id++, now, 1, 1);

		done();
	});

	after(function () {
		delete_all_authUsers(conn);
		conn.end();
    });

	it('test clean authUsersTable', function (done) {
		// before run clean authUsers Table utility
		query_authUsers_RowCount(conn, 8);

		var exec = require('child_process').exec;
		exec('node ../util/clean_authUserTable.js -t', function (error, stdout, stderr) {
			if (error) {
				console.log(stderr);
				throw error;
			} else {
				// after run clean authUsers Table utility
				console.log(stdout);
				query_authUsers_RowCount(conn, 3);
				query_authUsers_ids(conn);
			}
			done();
		});
    });
});