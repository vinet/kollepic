var request = require('supertest');
var should = require('should');
var test_util = require('./test_util');
var authDao = require('../dao/authDao');

describe('photofeed rest api tests', function() {
    var url = 'http://localhost:3000';
    var userId = 1113941505;
    // assign a constant auth_token and set time to future
    var auth_token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE0NDA2NjU2NDgsImlzcyI6IjExMTM5NDE1MDUifQ.2XezwIYvmeZkQbtSNxSA5jstPlgd9b50DAxPfRLqWxo';
    var access_token = '1113941505.08d8fe3.0866c2fc818c4324b9380e205af2b3e0';

    // for search api
    var search_user = 'grace';
    // for media api
    var media_id = '1059146550548371417_241615260';
    // for users recent update api
    var search_user_id = '321340967';

    before(function(done) {
        authDao.connectDB()
            .then(
                function(conn) {
                    return test_util.force_login(conn, userId, access_token, auth_token)
                        .then(function() {
                            conn.release();
                        });
                }
            ).then(function() {
                authDao.connectDB()
                .then(
                    function(conn) {
                        conn.release();
                        return authDao.exist(conn, userId, access_token, auth_token);
                    },
                    function(err) {
                        throw err;
                    }
                ).then(
                    function() {
                        done();
                    }, function(error) {
                        throw error;
                    });
            });
    });

    after(function() {
    	authDao.connectDB()
    	.then(
    		function(conn) {
    			return test_util.force_logout(conn, userId, auth_token)
    			.fin(function(){
    				conn.release();
    			});
    		})
    	.then(
    		function() {
		        console.log('All tests are done');
    		},
    		function(err) {
    			console.log('clean user from DB unsuccessful');
		        console.log('All tests are done');
                throw err;
    		});
    });

    it('get feed self', function(done) {
        request(url)
            .get('/api/feed/self?count=12')
            .set('Authorization', auth_token)
            .expect(200)
            .expect('Content-Type', /json/)
            .end(function(err, res) {
                if (err) {
                    return done(err);
                }
                res.body.length.should.not.equal(0);
                done();
            });
    });

    it('get users self', function(done) {
        request(url)
            .get('/api/users/self')
            .set('Authorization', auth_token)
            .expect(200)
            .expect('Content-Type', /json/)
            .end(function(err, res) {
                if (err) {
                    return done(err);
                }
                res.body[0].id.should.be.exactly(userId).and.be.a.Number();
                res.body[0].should.have.property('username', 'test_user');
                res.body[0].should.have.property('full_name', 'test user');
                res.body[0].should.have.property('provider', 'instagram');
                done();
            });
    });


    it('GET /users/self/media/liked?count=12', function(done) {
        request(url)
            .get('/api/users/self/media/liked?count=12')
            .set('Authorization', auth_token)
            .expect(200)
            .expect('Content-Type', /json/)
            .end(function(err, res) {
                if (err) {
                    return done(err);
                }
                res.body.length.should.not.equal(0);
                done();
            });
    });

    // test more with order by different parameter
    // DB start with empty
    it('GET /users/self/favorite', function(done) {
        request(url)
            .get('/api/users/self/favorite')
            .set('Authorization', auth_token)
            .expect(200)
            .expect('Content-Type', /json/)
            .end(function(err, res) {
                if (err) {
                    return done(err);
                }
                res.body.length.should.equal(0);
                //res.body.length.should.not.equal(0); should equal to a number of fav photos I set in DB
                done();
            });
    });

    // should keep login after 30 min, update DB and send and GET request again
    it('POST /users/self/remember', function(done) {
        request(url)
            .post('/api/users/self/remember')
            .set('Authorization', auth_token)
            .expect(200)
            .end(function(err) {
                if (err) {
                    return done(err);
                }
                test_util.increase_login_timestamp(userId).then(
                    function() {
                        request(url)
                            .get('/api/users/self')
                            .set('Authorization', auth_token)
                            .expect(200)
                            .expect('Content-Type', /json/)
                            .end(function(err, res) {
                                if (err) {
                                    return done(err);
                                }
                                res.body[0].id.should.be.exactly(userId).and.be.a.Number();
                                res.body[0].should.have.property('username', 'test_user');
                                res.body[0].should.have.property('full_name', 'test user');
                                res.body[0].should.have.property('provider', 'instagram');
                                done();
                            });
                    });
            });
    });


    it('GET /users/search', function(done) {
        request(url)
            .get('/api/users/search?q=' + search_user)
            .set('Authorization', auth_token)
            .expect(200)
            .expect('Content-Type', /json/)
            .end(function(err, res) {
                if (err) {
                    return done(err);
                }
                res.body[0].should.have.property('username', 'miugrace');
                done();
            });
    });


    // test specific user_id and should not return empty
    it('GET /users/:user_id', function(done) {
        request(url)
            .get('/api/users/321340967')
            .set('Authorization', auth_token)
            .expect(200)
            .expect('Content-Type', /json/)
            .end(function(err, res) {
                if (err) {
                    return done(err);
                }
                res.body.should.have.property('username', 'miugrace');
                res.body.should.have.property('full_name', 'Grace');
                done();
            });
    });

    // test specific user_id and should not return empty
    it('GET /media/:media_id/comments', function(done) {
        request(url)
            .get('/api/media/' + media_id + '/comments')
            .set('Authorization', auth_token)
            .expect(200)
            .expect('Content-Type', /json/)
            .end(function(err, res) {
                if (err) {
                    return done(err);
                }
                res.body.should.not.equal(0);
                res.body[0].should.have.property('id', '1059208360655852628');
                done();
            });
    });

    // fav_photo DB start with empty, length should equal to one after this request
    it('POST /media/:media_id/favorite', function(done) {
        request(url)
            .post('/api/media/' + media_id + '/favorite')
            .set('Authorization', auth_token)
            .expect(200)
            .end(function(err) {
                if (err) {
                    return done(err);
                }
                done();
            });
    });

    it('GET /media/:media_id/favorite after favrited one photo', function(done) {
        request(url)
            .get('/api/media/' + media_id + '/favorite')
            .set('Authorization', auth_token)
            .expect(200)
            .expect('Content-Type', /json/)
            .end(function(err, res) {
                if (err) {
                    return done(err);
                }
                res.body.should.have.property('result', true);
                done();
            });
    });

    // unfavorite photos
    it('POST /media/:media_id/unfavorite', function(done) {
        request(url)
            .post('/api/media/' + media_id + '/unfavorite')
            .set('Authorization', auth_token)
            .expect(200)
            .end(function(err) {
                if (err) {
                    return done(err);
                }
                done();
            });
    });

    it('GET /media/:media_id/favorite after unfavrited one photo', function(done) {
        request(url)
            .get('/api/media/' + media_id + '/favorite')
            .set('Authorization', auth_token)
            .expect(200)
            .expect('Content-Type', /json/)
            .end(function(err, res) {
                if (err) {
                    return done(err);
                }
                res.body.should.have.property('result', false);
                done();
            });
    });

    it('GET /media/:media_id', function(done) {
        request(url)
            .get('/api/media/' + media_id)
            .set('Authorization', auth_token)
            .expect(200)
            .expect('Content-Type', /json/)
            .end(function(err, res) {
                if (err) {
                    return done(err);
                }
                res.body.length.should.equal(1);
                done();
            });
    });

    it('GET /users/:user_id/media/recent', function(done) {
        request(url)
            .get('/api/users/' + search_user_id + '/media/recent?count=12')
            .set('Authorization', auth_token)
            .expect(200)
            .expect('Content-Type', /json/)
            .expect(function(res){
            	if(res.body[0].type === 'video')
            	{
            		res.body[0].type = 'image';
            	}
            })
            .end(function(err, res) {
                if (err) {
                    return done(err);
                }
                res.body.length.should.equal(12);
                res.body[0].should.have.property('type', 'image');
                done();
            });
    });
    // test specific user_id and should not return empty
    it('POST /users/self/logout', function(done) {
        request(url)
            .post('/api/users/self/logout')
            .set('Authorization', auth_token)
            .expect(200)
            .end(function(err) {
                if (err) {
                    return done(err);
                }
                done();
            });
    });

    // test specific user_id and should not return empty
    it('GET /users/self/ after LOGOUT', function(done) {
        request(url)
            .get('/api/users/self/')
            .set('Authorization', auth_token)
            .expect(403)
            .expect('Content-Type', /json/)
            .end(function(err, res) {
                if (err) {
                    return done(err);
                }
                res.body.should.have.property('message', 'Fail to authenticates user');
                done();
            });
    });
});
