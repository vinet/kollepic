#!/bin/sh

echo_time() {
     date +"%Y-%m-%d %H:%M:%S - $(printf "%s " "$@" | sed 's/%/%%/g')"
}

kill_server_if_applicable() {
	process_id=$(ps -e | grep "[n]ode" | awk '{print $1}')
	if [ -n "$process_id" ]; then
	    kill -9 $process_id
	    echo_time "kill node process "$process_id
	fi
}

export DB_TYPE='test'

kill_server_if_applicable

cd ..
node server.js &
sleep 3s
mocha tests/api.test.js

kill_server_if_applicable

cd tests/
mocha clean_authUserTable.test.js
export DB_TYPE='dev'