# Kollpic API Doc

## Authentication
Authentication apis does not need to have API authentication token. All other APIs need this token
> POST /api/auth/instagram  
> Login to PhotoFeed app and get instagram access_token  
  
If the user does not login to Instagram, redirect to instagram login page.  
Response parameter access\_token is used to call instagram APIs  
Response parameter token is PhotoFeed app API authentication token.  

Example:
```json
{
	"user": {
		"username": "(instagram username)",
		"bio": "",
		"website": "",
		"profile_picture": "(instagram profile image url))",
		"full_name": "(instagram full_name)",
		"id": "(instagram id)",
		"provider": "instagram"
	},
	"token":"..."
}
```

## Feed
> GET /api/feed/self  
> Get login user self photo feed  
  
Header: Authorization=TOKEN  
Parameters:  
count:  count of media to return  
max_id: return media earlier than this max_id  
min_id: return media later than this min_id  

Response:  
user_favorite: true if this photo is selected as a favorite photo  

## User
> GET /api/users/self  
> Get login user profile  
  
Header: Authorization=TOKEN  

Example:  
```json
{
	"id": "(instagram id)",
	"username": "",
	"profile_picture": "",
	"full_name": "",
	"provider": "instagram"
}
```

---
> POST /users/self/logout  
> Logout current user.  

Header: Authorization=TOKEN  

Call this API, when user clicks logout.

---
> POST /api/users/self/remember  
> Enable remember me feature if the user selects remember_me when login  
  
Header: Authorization=TOKEN  

Call this API, when user selects remember me when login.

---
> GET /api/users/self/media/liked  
> Get the user's list of liked media  
  
Header: Authorization=TOKEN  
Parameters:  
count:  count of media to return  
max_id:  return media earlier than this max_id  
  
Response:  
user_favorite: true if this photo is selected as a favorite photo  
  
---
> GET /api/users/:user_id  
> GET get user detail by user_id  

```json
{
    "data": {
        "id": "1574083",
        "username": "snoopdogg",
        "full_name": "Snoop Dogg",
        "profile_picture": "http://distillery.s3.amazonaws.com/profiles/profile_1574083_75sq_1295469061.jpg",
        "bio": "This is my bio",
        "website": "http://snoopdogg.com",
        "counts": {
            "media": 1320,
            "follows": 420,
            "followed_by": 3410
        }
}

```
Header: Authorization=TOKEN  
get a user's detail by user_id, if user_id equals to 'self', this will return his own detail.  
put user_id in request url  

## Media
> GET /api/media/{media_id}/comments  
> Get a list of recent comments on a media object  
  
Header: Authorization=TOKEN  
Put {media_id} in request URL  
---
> GET /api/media/{media_id}  
> Get detailed information on a media object  
  
Header: Authorization=TOKEN  
Put {media_id} in request URL  

Response:  

## Favorite
> GET /api/users/self/favorite  
> GET one page of the current authorized user favorite photots  

Return the authenticated user's favorite photos  
Return photos sorted by the time or fav_time when the photo is selected as favorite photo.  
Pagination is provided in response json next_page_begin attribute  
fav_timestamp_unix is in milisecond

Default one page photos count is 12  
  
Header: Authorization=TOKEN  
Parameters:  
sort: favorite photos sort argument, default sort by favorite time  
	'created_at': sort by created time  
	'create_user_name': sort by create user name  
	'favorited_at': sort by favorited time  
  
begin: favorite photos beginning index  
  
Response:  
user_favorite: true if this photo is selected as a favorite photo  

```json
[
  {
    "userId": 1113941505,
    "mediaId": "1061272213573080709_241615260",
    "fav_timestamp": "2015-08-28T10:26:56.000Z",
    "low_resolution": "",
    "thumbnail": "",
    "standard_resolution": "",
    "type": "image",
    "video_standard_resolution": "",
    "create_timestamp": "2015-08-28T03:45:28.000Z",
    "create_userId": 241615260,
    "create_username": "shikiie",
    "rating": -1,
    "fav_timestamp_unix": 1440757616
  }
]
```
---
> POST /api/media/:media_id/favorite  
> Select one media as a favorite media  

Header: Authorization=TOKEN  
Put {media_id} in request URL  
  

---
> POST /media/:media_id/unfavorite  
> Unselect one media as a favorite media  

Header: Authorization=TOKEN  
Put {media_id} in request URL  
---
> GET /media/:media_id/favorite  
> Check if this media is selected as favorite photo  

Header: Authorization=TOKEN  
Put {media_id} in request URL  

```json
{
	"result": true	// false if not exist
}
```
---

> Get /api/users/{user_id}/media/recent  
> get most recent media from user(user_id)  

Header: Authorization=TOKEN  
Put {user_id} in request URL  

Parameters:  
count:  count of media to return  
max_id: return media earlier than this max_id  
min_id: return media later than this min_id  
max_timestamp: return media before this timestamp  
min_timestamp: return media after this timestamp  

## Search
> Get /api/search   (This API is bad. Will rewrite!)
> get search result base on {tags/locations/followed-by/relationship/etc}

Header: Authorization=TOKEN  
Parameter:  
    op: {tag|location} REQUIRED (for now)  
    tag: required if op = tag  
    longitude: required if op = location  
    latitude: required if op = location  
  
    MORE TO GO:  
        location_id: may combine with tag, search media with tag "tag" around location by id  
        location_name: may combine with tag, search media with tag "tag" around location by name  

---  
> GET /api/users/search  
> search users by user_name, it might return more than one user  

params:  
	q: username that user looking for  

Example
```json
{
    "data": [{
        "username": "jack",
        "first_name": "Jack",
        "profile_picture": "http://distillery.s3.amazonaws.com/profiles/profile_66_75sq.jpg",
        "id": "66",
        "last_name": "Dorsey"
    },
    {
        "username": "sammyjack",
        "first_name": "Sammy",
        "profile_picture": "http://distillery.s3.amazonaws.com/profiles/profile_29648_75sq_1294520029.jpg",
        "id": "29648",
        "last_name": "Jack"
    },
    {
        "username": "jacktiddy",
        "first_name": "Jack",
        "profile_picture": "http://distillery.s3.amazonaws.com/profiles/profile_13096_75sq_1286441317.jpg",
        "id": "13096",
        "last_name": "Tiddy"
    }]
}
```