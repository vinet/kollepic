# Git summary

## Git Config

### General

As a minimum, you need to specify your username and email:

> git config --global user.name <your-name>
> git config --global user.email <your-email>

Example:

> git config --global user.name "John Doe"
> git config --global user.email "jdoe@vmware.com"

It is advisable to make enable the output coloring:

> git config --global color.ui true

Make sure that git push does the correct thing. The simple behavior will push only the current branch to the remote repository and only if it's name matches the remote one.


> git config --global push.default simple

## Git Ignore

The .gitignore file describes ignore patterns that are applied recursively on the directory and directories below it. It is handy to use nested git ignore files for more reasonable separation of functionality.

### Setup

Download the Git Ignore (click) and save it to your home directory.

Set up ~/gitignore.txt file as a global exclude list:


> git config --global core.excludesfile ~/gitignore.txt

### Commit Template

The commit template will automatically fill all the required fields in you commit messages that are required by the git policy (e.g. bug, reviewer, etc.). The format of the git commits should be:
```
<put a good 1-line overview here!>

<put your multi-line change description here!>

Review URL: <required>
Test: <required>
QA Notes:
```

It is important to have empty line between the short and log description - otherwise git cannot differentiate them and too long lines are shown in the git history.

Create a file inside the .git folder of the repository, named commit_template.txt
Note: if you don't see the .git folder, change your Windows Explorer to show hidden files.
Paste the Commit Message Template from the Quick Links section (don't forget to strip the Description: headline and adjust the indention of the lines)
Configure the template

> git config commit.template .git/commit_template

This should be done for each repository. You can do it globally, but it will be applied to all your Git projects, so it might not be what you want. If you insist on a global setting, the procedure is the same, but:

Place the file somewhere in your home folder
Configure the template globally:

> git config --global commit.template <path-to-the-template-file>

## Diff and Merge tools

### Windows

* Setup Beyond Compare as visual diff and merge tool

> git config --global merge.tool bc3
> git config --global mergetool.bc3.path "c:\program files (x86)\beyond compare 3\bcomp.exe"
> git config --global diff.tool bc3
> git config --global difftool.bc3.path "c:\program files (x86)\beyond compare 3\bcomp.exe"

* Setup p4merge as visual diff and merge tool


> git config --global merge.tool p4merge
> git config --global mergetool.p4merge.cmd "'p4merge.exe \"$BASE\" \"$LOCAL\" \"$REMOTE\" \"$MERGED\"'"
> git config --global diff.tool p4merge
> git config --global difftool.p4merge.cmd "'p4merge.exe \"$BASE\" \"$LOCAL\" \"$REMOTE\" \"$MERGED\"'"


* Setup Araxis Merge as visual diff and merge tool

Update the appropriate Git configuration file by adding the following diff and merge tool configurations:

[diff]
tool = araxis
[difftool "araxis"]
path = "/C/Program Files/Araxis/Araxis Merge/Compare"
[merge]
tool = araxis
[mergetool "araxis"]
path = "/C/Program Files/Araxis/Araxis Merge/Compare"

### Mac OS

* Setup p4merge as visual diff and merge tool

> git config --global merge.tool p4mergetool
> git config --global mergetool.p4mergetool.cmd \
     "/Applications/p4merge.app/Contents/Resources/launchp4merge \$PWD/\$BASE \$PWD/\$REMOTE \$PWD/\$LOCAL \$PWD/\$MERGED"
> git config --global mergetool.p4mergetool.trustExitCode false
> git config --global mergetool.keepBackup false
> git config --global diff.tool p4mergetool
> git config --global difftool.p4mergetool.cmd \
      "/Applications/p4merge.app/Contents/Resources/launchp4merge \$LOCAL \$REMOTE"

The second folder contains various utilities, which are handy when working with Git command line.

## Useful commands

###Viewing Current Configurations

> git config --list

## Guidelines

Name     Description
Always rebase before merge     This would be enforced by the repository itself, but we should always rebase and keep a linear history in our main branch except for the cases when we merge server branches (i.e. branches used for releases). Because we try to minimize the use of such branches, we'll have relatively small number of merge commits in the repository (a merge commit is a commit marks two branches as becoming one, i.e. they are merged).
Commit message structure     Git pays special attention to the first line of the commit message as it's using it when displaying the history. The format of the message is the following:

```
<short one description> - give good summary of the commit
<blank line>
<long description> - gives all the details about the commit, what is
the problem it's solving, how it was solved and why it was solved
this way (possible alternatives).
<blank line>
Testing Done:
Reviewed By:
...
```

The short description should fit in one line and should not exceed 50-70 characters. The description should be written in present tense, i.e. 'Add support for user authentication against SSO' rather than 'Added support for user authentication against SSO'. The long description should be separated in paragraphs and it should be split in 72 characters. Most Git commit message editors will provide a margin to follow when writing the commit message.

Split your change     Try to split your work in as many as possible sane pieces, which are put for review. Use the power Git is giving you and specifically pick each line of each file, which you want to go in particular commit. Most important is two split the functional changes from the non-functional changes. Functional change is everything which changes the code is doing as end result. Non-functional changes is all straight-forward changes like method renames, formatting of the code, moving classes in another package, changing method signature, etc. Most code refactoring is non-functional changes, that's why they are automated by the IDEs. Everything else is considered functional change.

## Workflow

Checkout the repository
```
git clone ssh://git@git.eng.vmware.com/vcac.git vcac
cd vcac
git config merge.ff only
git config branch.autosetuprebase always
git config branch.master.rebase true
```

Note: on some systems, you might see the following:

ERROR   : Fail to download configuration of repo "vcac" from git.eng.vmware.com. Is the current repo configured?
ERROR   : 'NoneType' object is unsubscriptable

You have to disable the usage of the proxy for contacting git.eng.vmware.com from Python scripts, before proceeding:

> $ export no_proxy="eng.vmware.com"

Don't forget to execute the config commands as well. They'll help you think for less things.

Create new branch whenever you start working on new functionality

> git checkout -b <branch-name>

When finished, commit everything in the local branch (you can split it in several commits)
Checking what changes you have:

> git status

And

> git diff

Specify which files you want to add with:

> git add <file><file> <file> ...

Or add everything

> git add .

Commit the changes in the local repository

> git commit

Sometimes after committing you remember that you can add something more to the message or you've forgot a file:

> git commit --amend

will override the previous commit.
Checkout the master branch

> git checkout master

Pull the latest changes from the remote repository

> git checkout master
> git pull --rebase

Rebase your branch on the new main branch

> git checkout <branch-name>
> git rebase master

After successful preflight, update your commit message with the preflight URL and anything else you need:

> git commit --amend

If you have more than one commit on the same preflight, you'll need to use:

> git rebase <starting-commit> --interactive

To change the messages of all commits.
After all the info is provided in the commit message, merge it to master:

> git checkout master
> git merge <branch-name>

Make sure you see text like that:

```
[17:27] ~/Projects/vcac.git (master)$ git merge extension_interfaces
Updating 7a9a9ea..3c3a6a0
Fast-forward
build-resources/maven-settings.xml | 8 ++++++++
1 file changed, 8 insertions(+)
```
And not something like that:

```
[17:36] ~/Projects/vcac.git (master)$ git merge --no-ff extension_interfaces
Auto-merging platform/gwt-framework/src/main/filtered-resources/com/vmware/vcac/platform/framework/Framework.gwt.xml
Merge made by the 'recursive' strategy.
build-resources/maven-settings.xml                                                                                     |  8 ++++++++
platform/gwt-framework/pom.xml                                                                                         | 20 ++++++++++++++++++++
platform/gwt-framework/src/main/{resources => filtered-resources}/com/vmware/vcac/platform/framework/Framework.gwt.xml | 13 +++----------
3 files changed, 31 insertions(+), 10 deletions(-)
rename platform/gwt-framework/src/main/{resources => filtered-resources}/com/vmware/vcac/platform/framework/Framework.gwt.xml (81%)
```

If it's needed, rebase your branch and do it again
Push the changes to the remote repository

> git push

Stash your changes

> git stash

Git can continue multiple changes in stash. Each time you execute the above command, new entry in the stash is done.
Checkout/merge/push/pull/etc. whatever you need
When you are done, switch back to the old branch and apply the stash

> git stash apply

This will take the last stash entry you've added and reapply it. If you have many entries, you can use:

> git stash list

And then:

> git stash apply stash@{2}

To apply older entries from the stash.
Apply review request diff

This is done in very limited cases, but sometimes you may need to take a review request and apply it locally.

Download the diff from the review request
Optionally, check whether the patch will apply cleanly:

> git apply --check <patch-file>

Apply the patch:

> git apply -3 <patch-file>

The diff files from review board contains the commit ids, which were used for preparing the patch, so if your HEAD is different, Git will try to do a 3-way merge.

Git make and apply a patch

git apply patch with 3-way merge 

To generate your patch do the following:
> git format-patch --stdout first_commit^..last_commit > changes.patch
or
>git format-patch --stdout -n HEAD > changes.patch     (Create a patch from HEAD to the last n commits)

Now when you are ready to apply the patches:
> git am -3 < changes.patch

the -3 will do a three-way merge if there are conflicts. At this point you can do a git mergetool if you want to go to a gui or just manually merge the files using vim (the standard <<<<<<, ||||||, >>>>>> conflict resolution). 

### Miscellaneous Commands

It's best if you have a look at some cheat sheet, which list some necessary commands like:

Pending changes
```
$ git status
$ git diff
$ git difftool
```

Selecting changes to be part of the next commit

```
$ git add <file/directory> ...
$ git rm <file/directory> ...
```

Seeing difference between commits

> $ git diff <starting>..<ending>

starting and ending can be HEAD, branch names, tag names, commit SHA1.
Seeing history
```
$ git log
$ git log --oneline
$ git log <file/directory>
```

Seeing who contributed to a file

```
$ git blame <file>
$ git blame <commit-id>^ -- <file>
```

The second command is if you want to see annotated file before a certain commit.
Purging files

```
$ git clean -f # clean all untracked files, but excluding the ignored
$ git clean -f -d # clean all untracked files and empty directories
$ git clean -f -x # clean all untracked files and ignored one as well (build artifacts will be deleted)
```

Backout/revert a commit

> $ git revert --no-commit <commit-id>

Discard current changes

```
$ git reset --hard # discard pending changes
$ git reset --hard <commit-id> # discards commits
$ git reset --soft  <commit-id> # resets the branch pointer to specific commit, but do not change the files
$ git reset # discards things prepared for commit, but you can start over
```

Revert specific files

> $ git checkout <file> <file> ...

Ignore locally modified file, which should never be submitted:
```
$ git update-index --assume-unchanged <file>
$ git update-index --no-assume-unchanged <file>
```

The second command reverts the first one, when you need. You'll need this if you have a change in the file you want to submit or when someone from outside changed the file: Git will complain if this file is ever changed from a commit.
Check how many commits you have ahead of origin master - useful when you want to squash X number of commits together

> $ git rev-list origin..HEAD --count

squash/ "merge" commits together

> $ git rebase -i HEAD~<number of commits you have above head> or use
> $ git rebase <starting-commit> --interactive

## Tips & Tricks

### Speeding up Git

Enable preload index

> git config --global core.preloadindex true

This speeds up git by enabling parallelism to git diff operations ( e.g. git status )

Run git bash as Administrator ( Windows )
Use standard command prompt ( Windows )
Use a local git installation
Install git outside the "Program Files" folder ( Windows )
This prevents some UAC control issues

Disabling UAC and/or LUAFV ( Windows )
UAC and the driver responsible - LUAFV tend to slow down git on some systems. Disabling them is a last resort option, because it might create security issues.

### Better Git Log

Usage :

> git log --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit

Creating an alias :

> git config --global alias.lg "log --color --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit"

Further Usage :
View Log :

> git lg

View Changed Lines :

> git lg -p

### Short Format Git Status

Short format guide (click).

Usage :

> git status -sb

Creating an alias :

> git config --global alias.st "status -sb"

Further Usage :

> git st

### Miscellaneous

You can specify branch names containing '/'. GUI clients will organize them in folders according to their names.
You can easily refer to relative branches using ^ to refer to the branch before the specified one like this:

> $ git checkout HEAD^

will checkout the commit before the current one
Or

> $ git checkout HEAD^^

will checkout two commits before that.
HEAD can be any valid revision specifier like branch names, tags and commit SHA1.

### Git Autocorrect

Git can handle most misspellings when configured.

> git config --global help.autocorrect 1

Using this will automatically run an incorrectly spelled command if it finds a single match for it. E.g git comit will work.

The value 1 is not necessary - it tells git to wait for 0.1 seconds before applying the corrected command - this time could be modified.

### Git reflog

Git reflog keeps track of the commits HEAD points to after each commit. It's handy, when you commit something, try to do something and then you have troubles getting to the stage before you've started experimenting. Use reflog to recover the state.

### The Ultimate History Editor

Here is how to manipulate the history of your local branch (let's say you want to reorganize the commits in more meaningful fashion or move some bits around):

> $ git rebase <commit-id> --interactive

The commit-id is the first commit, which you don't want to modify going back in time. More information here.

### Ignoring directories

Ignoring whole directories could be done in a easily by adding a .gitignore file with just an asterisk inside. Additionally the file could be easily copied anywhere and the directory renamed without a problem.

### Git rerere

When you have changes, which you are working on it you might end up having to merge multiple times and the way you merge is always the same. You can use the git rerere command to record a repetitive merge actions for specific files and specific parts of the file, so it will always resolve the same way.

More information here.

### Git bisect

Let's say something is broken, but it's really hard to understand what's causing the break. You can use Git bisect to do a binary search inside the commits. You specify the good commit and the bad commit and Git will start proposing commits between the good and the bad one until it finds, which exactly commit broke the things.

Note: to be useful, you need to have a relatively quick way to test whether the problem is present or not like using unit tests. If you have to spend 10-20 min updating the environment and reproducing the problem, then this is not a useful tip. Sorry

More information here.

## Troubleshooting

[edit]Migration

Tracking the migration.

[edit]New Possibilities

These are just ideas and there is no clear way of how it will exactly work, but there are no technical challenges for them right now, just a matter of implementation.

Sharing Changes
Git makes it really easy to share code between developers without going through the main repository. The following flow should be possible
Developer A works on a change and submits in its private repository
Developer B picks the change from A's private repository, review, make small adjustments and post it for review
Developer A comes back, see the review approve and start the preflight and then submit the change
[edit]Git feature branch collaboration workflow

Collaboration is coordinated and one-way isolated via a feature or topic branch. Features or Topics can be narrow or broad is scope, but it's best to keep them narrow in order to minimize the potential destabilization that can occur from an unanticipated impact on a separate feature when merged to master.

You name your feature branch like so: feature/<username>/<release-name>/<feature-name>. For example:


feature/pkennedy/altair/R3345-node-config-part-1

At the most basic level, collaboration among developers involves using the feature branch as the integration point for independent changes. The changes to be integrated fall into three categories:

Local (un-pushed) changes on the feature branch
Remote changes on the feature branch
Remote changes on the mainline branch (master)
We'll call these changes local, remote, and mainline respectively. To maintain a clean and readable commit history, I recommend the following workflow when integration various combinations of these categories:

Use rebase to integrate local changes with remote changes
Use merge to integrate the mainline with your feature changes
When you're ready to push your feature branch to the mainline, rebase the feature branch on top of the mainline branch then push to the mainline.
The following examples assume a feature branch of feature/pkennedy/altair/R3345-node-config-part-1 and a mainline of origin/master. Before looking at the examples, take note of a mechanism that saves effort when repeatedly merging changes from the mainline into the feature branch, over the branch lifetime.

[edit]git rerere (reuse recorded resolutions)

Rebasing a branch with master several times can be painful, since we need to fix any conflicting merges with each rebase. Git can actually remember how conflicts were resolved, so that only new conflicts need to be dealt with after a rebase. See the section on Git rerere in #Tips_.26_Tricks above.

## Creating a feature branch

A feature branch is nothing more than a local topic/feature branch that is pushed to origin, from where your collaborators can fetch it and work with it. Create a feature branch like so:


1. get the latest changes from origin
> git fetch origin
2. be in master
> git checkout master
3. create the local feature branch
> git checkout -b feature/pkennedy/altair/R3345-node-config-part-1

make changes to the local feature branch source, add tests,
build, test and when satisfied with quality, push to origin.
if you just want to create the branch and push to origin,
so that it's immediately available to collaborators, that works too.

> git push origin feature/pkennedy/altair/R3345-node-config-part-1

## Integrating local changes with remote changes

The most basic collaboration is when you have made changes (commits) to your local feature branch that you want to integrate with changes made by a team mate to the feature branch. First, fetch your teammate's changes:


1. get the latest changes from origin
> git fetch origin

Then rebase your local changes onto the remote feature branch:


2. be in the local feature branch
> git checkout feature/pkennedy/altair/R3345-node-config-part-1
3. rebase
> git rebase origin/feature/pkennedy/altair/R3345-node-config-part-1
4. push the result
> git push origin feature/pkennedy/altair/R3345-node-config-part-1

At this point any changes you've made locally will be integrated with your teammates' changes, and pushed to origin so they can integrate with your changes.

Integrating with mainline

Periodically you'll want to bring the latest mainline into your feature branch, so you can keep your in-progress feature working with the latest production code. This is done using a merge:


# get the latest changes from origin
git fetch origin
# be in the local feature branch
git checkout feature/pkennedy/altair/R3345-node-config-part-1
# merge in mainline
git merge origin/master
# push the result
git push origin feature/pkennedy/altair/R3345-node-config-part-1

You must integrate local and remote feature changes (if any) immediately before doing this.

Note the use of git merge here rather than git rebase in this step. Rebasing, i.e. rewriting of history, on shared branches can be dangerous. If your collaborators have pulled from the feature branch, you rewrite the feature branch history, then push to origin, when they next fetch from origin, your collaborators could have a lot of work ahead of them.

[edit]Code Review of feature branch changes

Feature branch changes must be reviewed prior to pushing to mainline. If you've set up git rerere, (see above) you won't have to re-resolve any conflicts you resolved earlier when you previously merged the mainline into your feature branch.

[edit]Pushing to mainline

Once you've received and responded to any feedback, you need to do one last rebase and then you can push your changes to mainline:


1. get the latest changes from origin
git fetch origin
2. be in the local feature branch
git checkout feature/pkennedy/altair/R3345-node-config-part-1
3. rebase onto the mainline
git rebase origin/master
4. push to the mainline
git push origin feature/pkennedy/altair/R3345-node-config-part-1:master

At this point your changes have been integrated to the mainline and your feature branches are defunct, so you should delete them:


1. delete the remote branch
git push origin :feature/pkennedy/altair/R3345-node-config-part-1
2. delete the local branch
git branch -D feature/pkennedy/altair/R3345-node-config-part-1

[edit]Merging to Master

Merging changed or new code to master is an exercise that must take place only after steps have been taken to verify that merging the code to master will not undermine the stability of master, and thus your co-workers, who are dependent on the stability of master just as you are.

This quality verification exercise must be undertaken whether the changed or new code is coming from a shared feature branch that contains work on which several developers and QE engineers have collaborated, or on a bugfix branch tracking master that only one person has worked on. The exact steps taken differ for each of those two kinds of streams, but the principle remains the same: Master must remain high-quality and stable.
 
How to reset one file from last commit
> git reset HEAD^ {file}
> git commit —-amend

The the file is unstage in the repository


Fixing "There was a problem with the editor 'vi'"

> git config --global core.editor /usr/bin/vim