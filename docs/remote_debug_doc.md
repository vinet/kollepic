# Remote debug

## How to do remote debug against our AWS ubuntu instance  

1. Run node-inspector & on remote ubuntu  
  	node-debug adds --debug-brk by default.  
  	--debug-brk will pause node application on the first line.  
  
2. Run node --debug server.js & on remote ubuntu (do export NODE_TYPE='production' first)  
  
3. Paste http://54.148.194.15:8080/?ws=54.148.194.15:8080&port=5858 to Chrome address bar  
	54.148.194.15 is our AWS ubuntu instance public IP address.  
	Ignore "Error: Cannot find module '/usr/local/lib/node_modules/node-inspector/node_modules/v8-debug/build/debug/v0.5.2/node-v14-linux-x64/debug.node'" in the console. Remote debug still works with this error.  
	Now under Chrome Sources tab, you can see all node source codes and add breakpoint to debug the web application.  
  
## How to correctly install lastest version of node on ubuntu
n is a node helper module. It works like RVM for Ruby on Rails application.  
use n {Node_Version} to select the expected node version  

> sudo npm cache clean -f  
> sudo npm install -g n  
> sudo n stable  
> sudo ln -sf /usr/local/n/versions/node/{Node_Version}/bin/node /usr/bin/node  


