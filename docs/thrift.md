# Thrift Doc
Thrift online doc: https://thrift.apache.org/docs/  

## Compile thrift:  
	1. thrift RPC call IDL(interface definition language) in thrift/search_svc.thrift  
	2. Run thrift-gen.sh to generate thrift RPC stubs for search service and node web server   

## current interface for search service
* search interface  
```
list<string> search(1: string access_token, 2:map<string, string> query) throws (1:MissingParameterException err, 2:InvalidSearchOpException op_err)
```

parameters:
	1: access_token
	2: query: map<string,string>
		This is an object with many key-value, it contains all query params
		query map have to contains a key 'op'(for now), which is controlling which service(s) will
		be used.



