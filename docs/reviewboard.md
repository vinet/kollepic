# Reviewboard Doc

## Reviewboard AWS address
Reviewboard address: http://ec2-54-149-219-59.us-west-2.compute.amazonaws.com/  

## Reviewboard local setup
* Install RBTools  
> easy_install -U RBTools  
* Useful post review commands  
> rbt post {COMMIT_NUMBER}     Post a new review
> rbt post -r {REVIEWBOARD_Id} {COMMIT_NUMBER}   Update an existing review with {REVIEWBOARD_Id} 

* Install commit template to git
Commit_template download address: https://www.dropbox.com/s/e9hg8wbjiv65fgd/commit_template.txt?dl=0  
> git config commit.template ~/commit_template.txt  

## How to install Reviewboard
1. Follow the online doc https://www.reviewboard.org/docs/manual/2.0/admin/installation/linux/  

2. Install nginx web server instead of apache web server  
> sudo apt-get install nginx  

3. Start reviewboard with fastcgi  
> sudo easy_install flup  
> rb-site manage /var/www/reviewboard runfcgi method=threaded port=3033 host=127.0.0.1 protocol=fcgi  

4. Modify ngnix configuration as follows  
sudo vi /etc/nginx/sites-available/default  
```
server {
	listen 80;
	server_name ec2-54-149-219-59.us-west-2.compute.amazonaws.com;
	error_log /var/log/nginx/reviews.error_log warn;
	access_log /var/log/nginx/reviews.access_log combined;

	root /var/www/reviewboard/htdocs/;

	location / {
		try_files $uri @reviews_proxy;
	}

	location @reviews_proxy {
		fastcgi_pass 127.0.0.1:3033;
		fastcgi_param PATH_INFO $fastcgi_script_name;
		fastcgi_param REQUEST_METHOD $request_method;
		fastcgi_param QUERY_STRING $query_string;
		fastcgi_param CONTENT_TYPE $content_type;
		fastcgi_param CONTENT_LENGTH $content_length;
		fastcgi_pass_header Authorization;
		fastcgi_param SERVER_PROTOCOL $server_protocol;

		fastcgi_param GATEWAY_INTERFACE CGI/1.1;
		fastcgi_param SERVER_SOFTWARE nginx/$nginx_version;

		fastcgi_param REMOTE_ADDR $remote_addr;
		fastcgi_param REMOTE_PORT $remote_port;
		fastcgi_param SERVER_ADDR $server_addr;
		fastcgi_param SERVER_PORT $server_port;
		fastcgi_param SERVER_NAME $server_name;

		fastcgi_intercept_errors off;
	}
}
```

5. (Optional) To support sending emails when one review is posted  
* install smtp email server:  
> sudo apt-get install postfix  
> sudo vi /etc/postfix/main.cf (view postfix conf if needed)  
* Open AWS smtp port 25  

6. (Optional) Enable reviewboard logging  

7. (Optional) Reviewboard upgrade
> easy_install -U ReviewBoard

8. Trouble shooting  
reviewboard database:  
DB is mysql  
> use revewboard;  