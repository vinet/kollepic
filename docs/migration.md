# Database Migration

## Install db-migration
Run `npm install -g db-migrate` first to install db migration tool

## Database migration steps
* write migration file details link [npm db-migrate](http://db-migrate.readthedocs.org/en/latest/Getting%20Started/usage/#creating-migrations)
* create migration file: `db-migrate create add-email-to-users --config ./conf/database.json`
* Modify generated js file in migrations directory
* Run command `db-migrate up --config ./conf/database.json`. This will run all generated migration file if db migration did not happen before.

