# Mocha unit tests

## How to run backend tests
cd web_server/tests
./run_test.sh

## How to debug Mocha unit-tests

1. install node-inspector  
> npm install -g node-inspector  
  
2. In a separate Terminal window, run node-inspector with no arguments  
> node-inspector  
  
3. Go to http://127.0.0.1:8080/debug?port=5858 in Chrome.  
  
4. Run your mocha tests  
> mocha {test_file} --debug-brk  
--debug-brk will pause node application on the first line  
  
Manually open your test files with ⌘o (splat-o) or use the file browser on the left side of the inspector  