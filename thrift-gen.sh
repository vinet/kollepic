#!/bin/sh

rm -rf ./web_server/node-thrift && mkdir ./web_server/node-thrift
# generate code for server side
thrift --gen js:node --out ./web_server/node-thrift/ ./thrift/search_svc.thrift && echo "thrift: nodejs code has generated"

# install node modules
cd ./web_server && npm install
# compile and generate code for service side
cd ../search_svc/ && gradle clean build assemble



